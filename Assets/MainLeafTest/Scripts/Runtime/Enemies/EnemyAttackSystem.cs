﻿using System.Collections;
using UnityEngine;

namespace MainLeafTest.EnemyManagement
{
    public class EnemyAttackSystem : MonoBehaviour
    {
        protected virtual void Attack()
        {
            Enemy.Animator.SetTrigger(EnemyAnimationTriggers.ATTACK);
            CanAttack = false;
            StartCoroutine(ResetAttackCoroutine());
        }

        protected virtual void InitializeVariables()
        {
        }

        private void Awake()
        {
            InitializeVariables();
        }

        private void OnEnable()
        {
            DelayAttack();
        }

        private void DelayAttack()
        {
            CanAttack = false;
            StartCoroutine(ResetAttackCoroutine());
        }
        
        private IEnumerator ResetAttackCoroutine()
        {
            yield return new WaitForSeconds(Enemy.EnemyStats.AttackRate);
            CanAttack = true;
        }
        
        protected bool CanAttack { get; private set; }

        protected Enemy Enemy { get; set; }
    }
}