﻿using Cinemachine;
using UnityEngine;

namespace MainLeafTest.PlayerManagement.Camera
{
    public class CinemachinePOVExtension : CinemachineExtension
    {
        private const int CLAMP_ANGLE = 80;

        [SerializeField] private int _horizontalSpeed = 20;
        [SerializeField] private int _verticalSpeed = 15;
        
        private Vector3 _startingRotation;
        private Vector2 _mouseDelta;

        private bool _firstUpdate = true;
        
        public void SetMouseDelta(Vector2 mouseDelta)
        {
            _mouseDelta = mouseDelta;
        }
        
        protected override void Awake()
        {
            base.Awake();
            InitializeVariables();
        }

        protected override void PostPipelineStageCallback(CinemachineVirtualCameraBase vcam,
            CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
        {
            if (!vcam.Follow)
            {
                return;
            }

            if (stage != CinemachineCore.Stage.Aim)
            {
                return;
            }
            
            
            _startingRotation.x += _mouseDelta.x * Time.deltaTime * _verticalSpeed;
            _startingRotation.y += _mouseDelta.y * Time.deltaTime * _horizontalSpeed;

            if (_firstUpdate)
            {
                _startingRotation.x = 0;
                _startingRotation.y = 0;
                _firstUpdate = false;
            }
            
            _startingRotation.y = Mathf.Clamp(_startingRotation.y, -CLAMP_ANGLE, CLAMP_ANGLE);
            
            state.RawOrientation = Quaternion.Euler(-_startingRotation.y, _startingRotation.x, 0);
        }
        
        private void InitializeVariables()
        {
            _startingRotation = transform.localRotation.eulerAngles;
        }
    }
}