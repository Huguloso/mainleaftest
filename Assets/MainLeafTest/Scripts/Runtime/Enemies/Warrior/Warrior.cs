﻿using MainLeafTest.ObjectPoolerManagement;
using UnityEngine;
using UnityEngine.AI;

namespace MainLeafTest.EnemyManagement.WarriorManagement
{
    public class Warrior : Enemy
    {
        [SerializeField] private NavMeshAgent _navMeshAgent = default;

        [SerializeField] private WarriorStats _warriorStats = default;
        [SerializeField] private WarriorMovementSystem _warriorMovementSystem = default;
        [SerializeField] private WarriorAttackSystem _warriorAttackSystem = default;
        [SerializeField] private WarriorHealthSystem _warriorHealthSystem = default;
       
        private void Awake()
        {
            InitializeEnemyVariables();
        }

        private void InitializeEnemyVariables()
        {
            EnemyType = EnemyPoolNames.WARRIOR;
            
            EnemyStats = WarriorStats;
            EnemyMovementSystem = WarriorMovementSystem;
            EnemyHealthSystem = WarriorHealthSystem;
            EnemyAttackSystem = WarriorAttackSystem;
        }
        
        public NavMeshAgent NavMeshAgent => _navMeshAgent;
        public WarriorStats WarriorStats => _warriorStats;
        public WarriorMovementSystem WarriorMovementSystem => _warriorMovementSystem;
        public WarriorAttackSystem WarriorAttackSystem => _warriorAttackSystem;
        public WarriorHealthSystem WarriorHealthSystem => _warriorHealthSystem;

    }
}