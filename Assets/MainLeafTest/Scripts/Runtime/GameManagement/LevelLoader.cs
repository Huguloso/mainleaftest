﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MainLeafTest.GameManagement
{
    public class LevelLoader : MonoBehaviour
    {
        [SerializeField] private GameObject _loadingScreen = default;
        [SerializeField] private Slider _loadingProgressBar = default;
        
        public void LoadLevel(SceneIndexes scene)
        {
            StartCoroutine(LoadAsynchronously((int) scene));
        }

        private IEnumerator LoadAsynchronously(int sceneIndex)
        {
            AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

            _loadingScreen.SetActive(true);
            while (!operation.isDone)
            {
                _loadingProgressBar.value = operation.progress;
                yield return null;
            }
        }
    }
}