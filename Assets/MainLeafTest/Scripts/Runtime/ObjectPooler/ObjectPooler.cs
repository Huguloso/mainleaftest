﻿using UnityEngine;
using System.Collections.Generic;
using MainLeafTest.ArrowManagement;
using MainLeafTest.EnemyManagement;
using MainLeafTest.PickUpManagement;
using UnityEngine.VFX;

namespace MainLeafTest.ObjectPoolerManagement
{
    public class ObjectPooler : MonoBehaviour
    {
        [SerializeField] private List<GameObjectPool> _gameObjectPoolList = default;
        [SerializeField] private List<ArrowPool> _arrowPoolList = default;
        [SerializeField] private List<EnemyPool> _enemyPoolList = default;
        [SerializeField] private List<AudioPool> _audioPoolList = default;
        [SerializeField] private List<PickUpPool> _pickUpPoolList = default;
        [SerializeField] private List<VFXPool> _vfxPoolList = default;

        private Dictionary<GameObjectPoolNames, Queue<GameObject>> _gameObjectPoolDictionary;
        private Dictionary<ArrowPoolNames, Queue<Arrow>> _arrowPoolDictionary;
        private Dictionary<EnemyPoolNames, Queue<Enemy>> _enemyPoolDictionary;
        private Dictionary<AudioPoolNames, Queue<AudioSource>> _audioPoolDictionary;
        private Dictionary<PickUpPoolNames, Queue<PickUp>> _pickUpPoolDictionary;
        private Dictionary<VFXPoolNames, Queue<VisualEffect>> _vfxPoolDictionary;
        
        public GameObject SpawnGameObjectFromPool(GameObjectPoolNames poolName, Vector3 position,
            Quaternion rotation, Transform parent = null)
        {
            if (!_gameObjectPoolDictionary.ContainsKey(poolName))
            {
                return null;
            }

            GameObject spawnedObject = _gameObjectPoolDictionary[poolName].Dequeue();

            InitializeSpawnedObject(spawnedObject, position, rotation, parent);

            _gameObjectPoolDictionary[poolName].Enqueue(spawnedObject);

            return spawnedObject;
        }
        
        public Arrow SpawnArrowFromPool(ArrowPoolNames poolName, Vector3 position,
            Quaternion rotation, Transform parent = null)
        {
            if (!_arrowPoolDictionary.ContainsKey(poolName))
            {
                return null;
            }

            Arrow spawnedArrow = _arrowPoolDictionary[poolName].Dequeue();

            InitializeSpawnedObject(spawnedArrow.gameObject, position, rotation, parent);

            _arrowPoolDictionary[poolName].Enqueue(spawnedArrow);

            return spawnedArrow;
        }

        public Enemy SpawnEnemyFromPool(EnemyPoolNames poolName, Vector3 position,
            Quaternion rotation, Transform parent = null)
        {
            if (!_enemyPoolDictionary.ContainsKey(poolName))
            {
                return null;
            }

            Enemy spawnedEnemy = _enemyPoolDictionary[poolName].Dequeue();

            InitializeSpawnedObject(spawnedEnemy.gameObject, position, rotation, parent);

            _enemyPoolDictionary[poolName].Enqueue(spawnedEnemy);

            return spawnedEnemy;
        }
        
        public AudioSource SpawnFromAudioPool(AudioPoolNames poolName, Vector3 position, Quaternion rotation, Transform parent = null)
        {
            if (!_audioPoolDictionary.ContainsKey(poolName))
            {
                return null;
            }

            AudioSource spawnedAudio = _audioPoolDictionary[poolName].Dequeue();

            InitializeSpawnedObject(spawnedAudio.gameObject, position, rotation, parent);

            _audioPoolDictionary[poolName].Enqueue(spawnedAudio);

            return spawnedAudio;
        }
        
        public PickUp SpawnFromPickUpPool(PickUpPoolNames poolName, Vector3 position, Quaternion rotation, Transform parent = null)
        {
            if (!_pickUpPoolDictionary.ContainsKey(poolName))
            {
                return null;
            }

            PickUp spawnedPickUp = _pickUpPoolDictionary[poolName].Dequeue();

            InitializeSpawnedObject(spawnedPickUp.gameObject, position, rotation, parent);

            _pickUpPoolDictionary[poolName].Enqueue(spawnedPickUp);

            return spawnedPickUp;
        }
        
        public VisualEffect SpawnFromVFXPool(VFXPoolNames poolName, Vector3 position, Quaternion rotation, Transform parent = null)
        {
            if (!_vfxPoolDictionary.ContainsKey(poolName))
            {
                return null;
            }

            VisualEffect spawnedVFX = _vfxPoolDictionary[poolName].Dequeue();

            InitializeSpawnedObject(spawnedVFX.gameObject, position, rotation, parent);

            _vfxPoolDictionary[poolName].Enqueue(spawnedVFX);

            return spawnedVFX;
        }
        
        private void Awake()
        {
            CheckForAnotherInstance();
        }

        private void CheckForAnotherInstance()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
            }
        }

        private void Start()
        {
            InitializeGameObjectPool();
            InitializeArrowPool();
            InitializeEnemyPool();
            InitializeAudioPool();
            InitializePickUpPool();
            InitializeVFXPool();
        }
        
        private void InitializeGameObjectPool()
        {
            _gameObjectPoolDictionary = new Dictionary<GameObjectPoolNames, Queue<GameObject>>();

            foreach (GameObjectPool pool in _gameObjectPoolList)
            {
                Queue<GameObject> objectsQueue = new Queue<GameObject>();
                for (int i = 0; i < pool.Size; i++)
                {
                    GameObject instantiatedObject = Instantiate(pool.Prefab);
                    instantiatedObject.SetActive(false);
                    objectsQueue.Enqueue(instantiatedObject);
                }

                _gameObjectPoolDictionary.Add(pool.PoolName, objectsQueue);
            }
        }

        private void InitializeArrowPool()
        {
            _arrowPoolDictionary = new Dictionary<ArrowPoolNames, Queue<Arrow>>();

            foreach (ArrowPool pool in _arrowPoolList)
            {
                Queue<Arrow> arrowsQueue = new Queue<Arrow>();
                for (int i = 0; i < pool.Size; i++)
                {
                    GameObject instantiatedObject = Instantiate(pool.Prefab);
                    instantiatedObject.SetActive(false);
                    Arrow instantiatedArrow = instantiatedObject.GetComponent<Arrow>();
                    arrowsQueue.Enqueue(instantiatedArrow);
                }

                _arrowPoolDictionary.Add(pool.PoolName, arrowsQueue);
            }
        }

        private void InitializeEnemyPool()
        {
            _enemyPoolDictionary = new Dictionary<EnemyPoolNames, Queue<Enemy>>();

            foreach (EnemyPool pool in _enemyPoolList)
            {
                Queue<Enemy> enemyQueue = new Queue<Enemy>();
                for (int i = 0; i < pool.Size; i++)
                {
                    GameObject instantiatedObject = Instantiate(pool.Prefab);
                    instantiatedObject.SetActive(false);
                    Enemy instantiatedEnemy = instantiatedObject.GetComponent<Enemy>();
                    enemyQueue.Enqueue(instantiatedEnemy);
                }

                _enemyPoolDictionary.Add(pool.PoolName, enemyQueue);
            }
        }
        
        private void InitializeAudioPool()
        {
            _audioPoolDictionary = new Dictionary<AudioPoolNames, Queue<AudioSource>>();

            foreach (AudioPool pool in _audioPoolList)
            {
                Queue<AudioSource> audioQueue = new Queue<AudioSource>();
                for (int i = 0; i < pool.Size; i++)
                {
                    GameObject instantiatedObject = Instantiate(pool.Prefab);
                    instantiatedObject.SetActive(false);
                    AudioSource instantiatedAudioSource = instantiatedObject.GetComponent<AudioSource>();
                    audioQueue.Enqueue(instantiatedAudioSource);
                }

                _audioPoolDictionary.Add(pool.PoolName, audioQueue);
            }
        }
        
        private void InitializePickUpPool()
        {
            _pickUpPoolDictionary = new Dictionary<PickUpPoolNames, Queue<PickUp>>();

            foreach (PickUpPool pool in _pickUpPoolList)
            {
                Queue<PickUp> pickUpQueue = new Queue<PickUp>();
                for (int i = 0; i < pool.Size; i++)
                {
                    GameObject instantiatedObject = Instantiate(pool.Prefab);
                    instantiatedObject.SetActive(false);
                    PickUp instantiatedPickUp = instantiatedObject.GetComponent<PickUp>();
                    pickUpQueue.Enqueue(instantiatedPickUp);
                }

                _pickUpPoolDictionary.Add(pool.PoolName, pickUpQueue);
            }
        }
        
        private void InitializeVFXPool()
        {
            _vfxPoolDictionary = new Dictionary<VFXPoolNames, Queue<VisualEffect>>();

            foreach (VFXPool vfxPool in _vfxPoolList)
            {
                Queue<VisualEffect> vfxQueue = new Queue<VisualEffect>();
                for (int i = 0; i < vfxPool.Size; i++)
                {
                    GameObject instantiatedObject = Instantiate(vfxPool.Prefab);
                    instantiatedObject.SetActive(false);
                    VisualEffect instantiatedVFX = instantiatedObject.GetComponent<VisualEffect>();
                    vfxQueue.Enqueue(instantiatedVFX);
                }

                _vfxPoolDictionary.Add(vfxPool.PoolName, vfxQueue);
            }
        }
        
        private void InitializeSpawnedObject(GameObject spawnedObject, Vector3 position, Quaternion rotation,
            Transform parent)
        {
            spawnedObject.SetActive(true);

            if (parent)
            {
                spawnedObject.transform.SetParent(parent);
            }
            else
            {
                spawnedObject.transform.SetParent(null);
            }
            
            spawnedObject.transform.localPosition = position;

            spawnedObject.transform.localRotation = rotation;

            IPoolableSpawn[] poolableComponents = spawnedObject.GetComponents<IPoolableSpawn>();

            if (poolableComponents == null)
            {
                return;
            }
            
            foreach (var poolableComponent in poolableComponents)
            {
                poolableComponent.OnPoolableSpawn();
            }
        }

        public static ObjectPooler Instance { get; private set; }
    }
}