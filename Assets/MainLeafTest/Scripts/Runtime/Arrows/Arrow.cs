﻿using MainLeafTest.GameManagement.CollisionManagement;
using MainLeafTest.HealthManagement;
using MainLeafTest.ObjectPoolerManagement;
using UnityEngine;
using UnityEngine.VFX;

namespace MainLeafTest.ArrowManagement
{
    public class Arrow : MonoBehaviour, IPoolableSpawn
    {
        [SerializeField] private Rigidbody _rigidbody = default;
        [SerializeField] private AudioClip _hitTargetSound = default;
        [SerializeField] private AudioClip _headShotSound = default;
        
        [SerializeField] private AudioClip[] _flyingSound = default;
        
        private ObjectPooler _objectPooler;
        
        private int _damage;
        private int _headShotDamage;
        
        private bool _hitSomething;
        
        public void Fire(int bowForce, int arrowDamage, int headShotDamage = 0)
        {
            _damage = arrowDamage;
            _headShotDamage = headShotDamage;
            
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.angularVelocity = Vector3.zero;
            
            transform.SetParent(null);

            _rigidbody.constraints = RigidbodyConstraints.None;
            _rigidbody.useGravity = true;
            _rigidbody.AddForce(-transform.up * bowForce, ForceMode.Impulse);

            PlayFlyingSound();
        }

        public void OnPoolableSpawn()
        {
            _hitSomething = false;
        }

        private void Awake()
        {
            InitializeObjectPooler();
        }

        private void InitializeObjectPooler()
        {
            _objectPooler= ObjectPooler.Instance;    
        }
        
        private void CheckColliderForDamageable(Collision other)
        {
            IDamageable damageable;
            
            if (other.gameObject.layer == (int)CollisionLayers.ENEMY_HEAD)
            {
                damageable = other.gameObject.GetComponentInParent<IDamageable>();
                damageable?.TakeDamage(_headShotDamage);
                PlayHeadshotImpactVFX();
                PlayImpactSound(_headShotSound);
                return;
            }
           
            damageable= other.gameObject.GetComponent<IDamageable>();

            if (damageable != null)
            {
                damageable.TakeDamage(_damage);
                PlayBodyImpactVFX();
                PlayImpactSound(_hitTargetSound);
            }
            else
            {
                SpawnStaticArrow();
            }
            
            PlaySmokeVFX();
        }

        private void Deactivate()
        {
            gameObject.SetActive(false);
        }
        
        private void PlayImpactSound(AudioClip audioClip)
        {
            if (!_hitTargetSound)
            {
                return;
            }
            
            AudioSource audioSource = _objectPooler.SpawnFromAudioPool(AudioPoolNames.DEFAULT_AUDIO_SOURCE,
                transform.position, Quaternion.identity);
            audioSource.PlayOneShot(audioClip);
        }

        private void PlayFlyingSound()
        {
            if (_flyingSound.Length == 0)
            {
                return;
            }
            
            int soundClip = Random.Range(0, _flyingSound.Length);
            AudioSource audioSource = _objectPooler.SpawnFromAudioPool(AudioPoolNames.DEFAULT_AUDIO_SOURCE,
                Vector3.zero, Quaternion.identity, transform);
            audioSource.PlayOneShot(_flyingSound[soundClip]);
        }

        private void PlaySmokeVFX()
        {
            VisualEffect smoke =
                _objectPooler.SpawnFromVFXPool(VFXPoolNames.IMPACT_SMOKE, transform.position, Quaternion.identity);
            smoke.Play();
        }
        
        private void PlayHeadshotImpactVFX()
        {
            VisualEffect impact =
                _objectPooler.SpawnFromVFXPool(VFXPoolNames.HEADSHOT_IMPACT_FLASH,transform.position, Quaternion.identity);
            impact.Play();
        }

        private void PlayBodyImpactVFX()
        {
            VisualEffect impact =
                _objectPooler.SpawnFromVFXPool(VFXPoolNames.BODY_IMPACT_FLASH,transform.position, Quaternion.identity);
            impact.Play();
        }
        
        private void SpawnStaticArrow()
        {
            _objectPooler.SpawnGameObjectFromPool(GameObjectPoolNames.STATIC_ARROW, transform.position,
                transform.rotation);
        }
        
        private void OnCollisionEnter(Collision other)
        {
            if (_hitSomething)
            {
                return;
            }
            
            _hitSomething = true;
            CheckColliderForDamageable(other);
            Deactivate();
        }
    }
}