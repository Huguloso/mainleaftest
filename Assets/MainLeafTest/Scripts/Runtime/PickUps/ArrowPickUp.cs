﻿using MainLeafTest.PlayerManagement;
using UnityEngine;

namespace MainLeafTest.PickUpManagement
{
    public class ArrowPickUp : PickUp
    {
        [SerializeField] private int _arrowAmount = 5;
        protected override void GetCollected(GameObject collector)
        {

            bool wasCollected = collector.GetComponent<PlayerBowSystem>().CollectArrows(_arrowAmount);

            if (!wasCollected)
            {
                return;
            }
            
            PlayCollectedSound();
            gameObject.SetActive(false);
        }
    }
}