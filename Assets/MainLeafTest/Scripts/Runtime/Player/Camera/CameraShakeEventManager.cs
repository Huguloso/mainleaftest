﻿using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace MainLeafTest.CameraShakeManagement
{
    public class CameraShakeEventManager : MonoBehaviour
    {
        private List<CameraShakeEvent> _cameraShakeEvents = new List<CameraShakeEvent>();

        private CinemachineBasicMultiChannelPerlin _cinemachinePerlin;

        private float _resultingAmplitude;
        private float _resultingFrequency;
        
        public void AddCameraShakeEvent(CameraShakeEventData data)
        {
            _cameraShakeEvents.Add(new CameraShakeEvent(data));
        }

        public void AddCameraShakeEvent(float amplitude, float frequency,
            float duration, AnimationCurve blendOverLifetime)
        {
            CameraShakeEventData data = ScriptableObject.CreateInstance<CameraShakeEventData>();
            data.Init(amplitude, frequency, duration, blendOverLifetime);
            
            _cameraShakeEvents.Add(new CameraShakeEvent(data));
        }

        private void Awake()
        {
            _cinemachinePerlin = GetComponent<CinemachineVirtualCamera>()
                .GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        }

        private void LateUpdate()
        {
            ExecuteShakeEvents();
        }
        
        private void ExecuteShakeEvents()
        {
            _resultingFrequency = 0;
            _resultingAmplitude = 0;

            for (int i = _cameraShakeEvents.Count - 1; i != -1; i--)
            {
                CameraShakeEvent shakeEvent = _cameraShakeEvents[i];

                shakeEvent.Update();

                _resultingAmplitude += shakeEvent.Intensity;
                _resultingFrequency += shakeEvent.Frequency;
                
                if (!shakeEvent.IsAlive())
                {
                    _cameraShakeEvents.RemoveAt(i);
                }
            }

            _cinemachinePerlin.m_AmplitudeGain = _resultingAmplitude;
            _cinemachinePerlin.m_FrequencyGain = _resultingFrequency;
        }
    }
}
