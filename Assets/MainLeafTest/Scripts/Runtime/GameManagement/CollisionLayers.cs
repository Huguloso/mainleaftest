﻿namespace MainLeafTest.GameManagement.CollisionManagement
{
    public enum CollisionLayers
    {
        PLAYER = 8,
        PLAYER_ARROW = 9,
        ENEMY = 10,
        ENEMY_ARROW = 11,
        ENEMY_HEAD = 12,
        CORPSE = 13
    }
}