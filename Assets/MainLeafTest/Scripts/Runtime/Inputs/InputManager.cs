﻿using System;
using UnityEngine;

namespace MainLeafTest.InputManagement
{
    public class InputManager : MonoBehaviour
    {
        public event Action<InputData> OnInputUpdate;

        private PlayerControls _playerControls;

        private void Awake()
        {
            CheckForAnotherInstance();
            InitializeVariables();
        }

        private void CheckForAnotherInstance()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
            }
        }

        private void InitializeVariables()
        {
            _playerControls = new PlayerControls();
        }

        private void OnEnable()
        {
            _playerControls.Enable();
        }

        private void Update()
        {
            InputData data = GetInputData();

            OnInputUpdate?.Invoke(data);
        }

        private void OnDisable()
        {
            _playerControls.Disable();
        }

        private InputData GetInputData()
        {
            InputData inputData;
            inputData.PlayerMovement = GetPlayerMovementInput();
            inputData.PlayerLook = GetMouseDelta();
            inputData.JumpInput = GetPlayerJumpInput();
            inputData.ShootInput = GetPlayerShootInput();
            inputData.PrepareArrowInput = GetPlayerPrepareArrowInput();
            inputData.PauseInput = GetPlayerPauseInput();
            return inputData;
        }

        private Vector2 GetPlayerMovementInput()
        {
            return _playerControls.Player.Movement.ReadValue<Vector2>();
        }

        private Vector2 GetMouseDelta()
        {
            return _playerControls.Player.Look.ReadValue<Vector2>();
        }
        
        private bool GetPlayerJumpInput()
        {
            return _playerControls.Player.Jump.triggered;
        }

        private bool GetPlayerPrepareArrowInput()
        {
            return _playerControls.Player.PrepareArrow.triggered;
        }
        
        private bool GetPlayerShootInput()
        {
            return _playerControls.Player.Shoot.triggered;
        }

        private bool GetPlayerPauseInput()
        {
            return _playerControls.Player.Pause.triggered;
        }
        
        public static InputManager Instance { get; private set; }
    }
}
