﻿using MainLeafTest.GameManagement.CollisionManagement;
using MainLeafTest.ObjectPoolerManagement;
using UnityEngine;

namespace MainLeafTest.PickUpManagement
{
    public class PickUp : MonoBehaviour
    {
        [SerializeField] private AudioClip _collectedSound = default;
        
        private ObjectPooler _objectPooler;
        
        protected virtual void GetCollected(GameObject collector)
        {
        }
        
        protected void PlayCollectedSound()
        {
            AudioSource audioSource = _objectPooler.SpawnFromAudioPool(AudioPoolNames.DEFAULT_AUDIO_SOURCE,
                transform.position, Quaternion.identity);
            
            audioSource.PlayOneShot(_collectedSound);
        }

        private void Start()
        {
            InitializeVariables();
        }

        private void InitializeVariables()
        {
            _objectPooler = ObjectPooler.Instance;    
        }
        
        private bool CheckIfIsPlayer(int layer)
        {
            return layer == (int) CollisionLayers.PLAYER;
        }
        
        private void OnTriggerEnter(Collider other)
        {
            if (!CheckIfIsPlayer(other.gameObject.layer))
            {
                return;
            }
            
            GetCollected(other.gameObject);
        }
    }
}