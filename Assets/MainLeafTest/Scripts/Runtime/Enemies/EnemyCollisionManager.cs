﻿using MainLeafTest.GameManagement.CollisionManagement;
using MainLeafTest.ObjectPoolerManagement;
using UnityEngine;

namespace MainLeafTest.EnemyManagement
{
    public class EnemyCollisionManager : MonoBehaviour, IPoolableSpawn
    {
        [SerializeField] private Enemy _enemy = default;
        [SerializeField] private GameObject _headCollider = default;
        
        public void OnPoolableSpawn()
        {
            EnableCollisions();
        }
        
        private void Start()
        {
            SubscribeToMethods();
        }

        private void SubscribeToMethods()
        {
            _enemy.EnemyHealthSystem.OnEnemyDeath += DisableCollisions;
        }

        private void EnableCollisions()
        {
            gameObject.layer = (int)CollisionLayers.ENEMY;
            _headCollider.layer = (int)CollisionLayers.ENEMY_HEAD;
        }

        private void DisableCollisions()
        {
            gameObject.layer = (int)CollisionLayers.CORPSE;
            _headCollider.layer = (int)CollisionLayers.CORPSE;
        }
    }
}