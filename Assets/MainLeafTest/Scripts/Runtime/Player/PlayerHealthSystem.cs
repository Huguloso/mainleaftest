﻿using System;
using MainLeafTest.HealthManagement;
using UnityEngine;

namespace MainLeafTest.PlayerManagement
{
    public class PlayerHealthSystem : HealthSystem, IHealable
    {
        public event Action OnPlayerDamaged;
        public event Action OnPlayerHealed;
        public event Action OnPlayerDeath;
        
        [SerializeField] private Player _player = default;
        
        public override void TakeDamage(int damage)
        {
            if (IsDead)
            {
                return;
            }
            
            base.TakeDamage(damage);
            OnPlayerDamaged?.Invoke();
        }

        public bool Heal(int healAmount)
        {
            if (Health == _player.PlayerStats.MaxHealth)
            {
                return false;
            }
            
            Health += healAmount;
            
            if (Health > _player.PlayerStats.MaxHealth)
            {
                Health = _player.PlayerStats.MaxHealth;
            }
            
            OnPlayerHealed?.Invoke();
            return true;
        }

        protected override void Die()
        {
            base.Die();
            OnPlayerDeath?.Invoke();
        }
        
        private void Awake()
        {
            InitializeVariables();
        }

        private void InitializeVariables()
        {
            Health = _player.PlayerStats.MaxHealth;
        }

        public bool LowHealth => Health < _player.PlayerStats.LowHealthThreshold;
    }
}