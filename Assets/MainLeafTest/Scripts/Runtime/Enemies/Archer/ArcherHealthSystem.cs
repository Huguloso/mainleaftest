﻿using UnityEngine;

namespace MainLeafTest.EnemyManagement.ArcherManagement
{
    public class ArcherHealthSystem : EnemyHealthSystem
    {
        [SerializeField] private Archer _archer = default;

        private void Awake()
        {
            InitializeVariables();
        }

        private void InitializeVariables()
        {
            Enemy = _archer;
        }
    }
}