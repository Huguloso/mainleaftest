﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MainLeafTest.HealthManagement
{
    public class HealthSystem : MonoBehaviour, IDamageable
    {
        public virtual void TakeDamage(int damage)
        {
            if (IsDead)
            {
                return;
            }
            
            Health -= damage;
            if (Health <= 0)
            {
                Die();
            }
        }

        protected virtual void Die()
        {
            IsDead = true;
        }


        public int Health { get; protected set; }

        public bool IsDead { get; protected set; }
    }
}