﻿namespace MainLeafTest.ObjectPoolerManagement
{
    public enum VFXPoolNames
    {
        IMPACT_SMOKE = 0,
        HEADSHOT_IMPACT_FLASH = 1,
        BODY_IMPACT_FLASH = 2
    }
}