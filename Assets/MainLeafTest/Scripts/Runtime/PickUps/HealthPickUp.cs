﻿using MainLeafTest.HealthManagement;
using UnityEngine;

namespace MainLeafTest.PickUpManagement
{
    public class HealthPickUp : PickUp
    {
        [SerializeField] private int _healthAmount = 20;
        protected override void GetCollected(GameObject collector)
        {
            base.GetCollected(collector);
            
            bool wasCollected = collector.GetComponent<IHealable>().Heal(_healthAmount);

            if (!wasCollected)
            {
                return;
            }
            
            PlayCollectedSound();
            gameObject.SetActive(false);
        }
    }
}