﻿using System;
using UnityEngine;

namespace MainLeafTest.ObjectPoolerManagement
{
    [Serializable]
    public struct PickUpPool
    {
        public PickUpPoolNames PoolName;
        public int Size;
        public GameObject Prefab;
    }
}