﻿using UnityEngine;

namespace MainLeafTest.EnemyManagement.ArcherManagement
{
    public class ArcherMovementSystem : EnemyMovementSystem
    {
        [SerializeField] private Archer _archer = default;
        
        private void Awake()
        {
            InitializeVariables();
        }

        private void InitializeVariables()
        {
            Enemy = _archer;
        }
    }
}