﻿using System.Collections;
using MainLeafTest.EnemyManagement.ArcherManagement;
using MainLeafTest.ObjectPoolerManagement;
using MainLeafTest.PlayerManagement;
using UnityEngine;

namespace MainLeafTest.EnemyManagement.SpawnManagement
{
    public class EnemySpawner : MonoBehaviour
    {
        private const float TIME_TO_OPEN_COFFIN = 1;
        
        [SerializeField] private EnemyWaveManager _waveManager = default;
        [SerializeField] private Player _player = default;
        [SerializeField] private CoffinSpawn[] _archerSpawnCoffin = default;
        [SerializeField] private CoffinSpawn[] _warriorSpawnCoffin = default;
        [SerializeField] private Material[] _warriorArmorColors = default;
        [SerializeField] private Material[] _archerArmorColors = default;
        
        private ObjectPooler _objectPooler;

        private int _currentArcherSpawnPoint;
        private int _currentWarriorSpawnPoint;
        
        public void SpawnEnemy(EnemyPoolNames enemyName)
        {
            switch (enemyName)
            {
                case EnemyPoolNames.ARCHER:
                    SpawnArcher();
                    break;
                case EnemyPoolNames.WARRIOR:
                    SpawnWarrior();
                    break;
            }
        }

        private void Start()
        {
            InitializeVariables();
        }

        private void InitializeVariables()
        {
            _objectPooler = ObjectPooler.Instance;
        }
        
        private void SpawnArcher()
        {
            _currentArcherSpawnPoint = Random.Range(0, _archerSpawnCoffin.Length);
            
            if (!CanSpawnArcher())
            {
                return;
            }

            Vector3 position = _archerSpawnCoffin[_currentArcherSpawnPoint].SpawnPoint.position;
            
            Enemy archer = _objectPooler.SpawnEnemyFromPool(EnemyPoolNames.ARCHER, position, Quaternion.identity);

            InitializeEnemy(archer);
        }

        private void SpawnWarrior()
        {
            _currentWarriorSpawnPoint = Random.Range(0, _warriorSpawnCoffin.Length);
            
            Vector3 position = _warriorSpawnCoffin[_currentWarriorSpawnPoint].SpawnPoint.position;
            Quaternion rotation = _warriorSpawnCoffin[_currentWarriorSpawnPoint].SpawnPoint.rotation;

            Enemy warrior = _objectPooler.SpawnEnemyFromPool(EnemyPoolNames.WARRIOR, position, rotation);

            InitializeEnemy(warrior);
        }

        private void InitializeEnemy(Enemy enemy)
        {
            enemy.Player = _player;
            enemy.WaveManager = _waveManager;

            enemy.gameObject.SetActive(false);
            
            if (enemy.EnemyType == EnemyPoolNames.ARCHER)
            {
                int armorColor = Random.Range(0, _archerArmorColors.Length);
                enemy.SetArmorColor(_archerArmorColors[armorColor]);
                
                _archerSpawnCoffin[_currentArcherSpawnPoint].PlayArcherAnimation(_archerArmorColors[armorColor]);
                
                _archerSpawnCoffin[_currentArcherSpawnPoint].OccupyPointWithArcher((Archer)enemy);
            }
            else
            {
                int armorColor = Random.Range(0, _warriorArmorColors.Length);
                enemy.SetArmorColor(_warriorArmorColors[armorColor]);
                
                _warriorSpawnCoffin[_currentWarriorSpawnPoint].PlayWarriorAnimation(_warriorArmorColors[armorColor]);
            }
            
            StartCoroutine(OpenCoffinCoroutine(enemy));
        }
        
        private bool CanSpawnArcher()
        {
            bool canSpawn = false;
            
            for (int i = 0; i < _archerSpawnCoffin.Length; i++)
            {
                if (_archerSpawnCoffin[_currentArcherSpawnPoint].OccupiedByArcher)
                {
                    _currentArcherSpawnPoint++;
                
                    if (_currentArcherSpawnPoint == _archerSpawnCoffin.Length)
                    {
                        _currentArcherSpawnPoint = 0;
                    }
                }
                else
                {
                    canSpawn = true;
                    break;
                }
            }
            return canSpawn;
        }

        private IEnumerator OpenCoffinCoroutine(Enemy enemy)
        {
            yield return new WaitForSeconds(TIME_TO_OPEN_COFFIN);
            enemy.gameObject.SetActive(true);
        }
    }
}