﻿using UnityEngine;

namespace MainLeafTest.EnemyManagement.WarriorManagement
{
    public class WarriorAudioManager : EnemyAudioManager
    {
        [SerializeField] private Warrior _warrior = default;

        [SerializeField] private AudioClip[] _swordHitSounds = default;
        [SerializeField] private AudioClip[] _swordMissSounds = default;

        [SerializeField] private AudioSource _footstepsSource = default;

        protected override void Start()
        {
            base.Start();
            SubscribeToEvents();
        }

        protected override void SubscribeToEvents()
        {
            base.SubscribeToEvents();
            _warrior.WarriorAttackSystem.OnWarriorAttackLanded += PlaySwordHitSound;
            _warrior.WarriorAttackSystem.OnWarriorAttackMissed += PlaySwordMissSound;
        }

        private void Awake()
        {
            InitializeVariables();
        }
        
        private void Update()
        {
            ManageFootstepsSound();
        }

        private void InitializeVariables()
        {
            Enemy = _warrior;
        }

        private void ManageFootstepsSound()
        {
            if (Time.timeScale == 0 && _footstepsSource.isPlaying)
            {
                _footstepsSource.Pause();
            }   
            else if (Time.timeScale == 1.0f && !_footstepsSource.isPlaying)
            {
                _footstepsSource.Play();
            }
        }
        
        private void PlaySwordHitSound()
        {
            if (_swordHitSounds.Length <= 0)
            {
                return;
            }
            
            int soundClip = Random.Range(0, _swordHitSounds.Length);
            AudioSource audioSource = SpawnAudioSource();
            audioSource.PlayOneShot(_swordHitSounds[soundClip]);
        }

        private void PlaySwordMissSound()
        {
            if (_swordMissSounds.Length <= 0)
            {
                return;
            }
            
            int soundClip = Random.Range(0, _swordMissSounds.Length);
            AudioSource audioSource = SpawnAudioSource();
            audioSource.PlayOneShot(_swordMissSounds[soundClip]);
        }
    }
}