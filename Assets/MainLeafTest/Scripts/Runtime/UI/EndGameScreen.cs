﻿using MainLeafTest.GameManagement;
using MainLeafTest.GameManagement.ScoreManagement;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

namespace MainLeafTest.UIManagement
{
    public class EndGameScreen : MonoBehaviour
    {
        [SerializeField] private SceneIndexes _currentScene = SceneIndexes.CEMETERY;
        [SerializeField] private Button _restartButton = default;
        [SerializeField] private Button _exitButton = default;
        [SerializeField] private LevelLoader _levelLoader = default;
        
        [SerializeField] private TextMeshProUGUI _pointsText = default;
        [SerializeField] private TextMeshProUGUI _enemiesText = default;
        [SerializeField] private TextMeshProUGUI _highScoreText = default;
        [SerializeField] private ScoreManager _scoreManager = default;

        [SerializeField] private MenuAudioManager _audioManager = default;
        
        protected void Start()
        {
            SubscribeToEvents();
        }

        private void OnEnable()
        {
            UpdateTexts();
        }

        private void UpdateTexts()
        {
            _pointsText.text = "Points:  " + _scoreManager.Points;
            _enemiesText.text = "Enemies killed: " + _scoreManager.EnemiesKilled;

            if (_highScoreText != null)
            {
                UpdateHighScore();
            }
        }
        
        private void SubscribeToEvents()
        {
            _restartButton.onClick.AddListener(() =>
            {
                _audioManager.PlayClickSound();
                Restart();
            });

            _exitButton.onClick.AddListener(() =>
            {
                _audioManager.PlayClickSound();
                ExitToMenu();
            });
        }

        private void UpdateHighScore()
        {
            if (_scoreManager.Points > PlayerPrefs.GetInt(PlayerPrefsKeys.HIGH_SCORE, 0))
            {
                PlayerPrefs.SetInt(PlayerPrefsKeys.HIGH_SCORE, _scoreManager.Points);
            }

            _highScoreText.text = "HighScore: " + PlayerPrefs.GetInt(PlayerPrefsKeys.HIGH_SCORE, 0);
        }
        
        private void Restart()
        {
            _levelLoader.LoadLevel(_currentScene);
        }

        private void ExitToMenu()
        {
            _levelLoader.LoadLevel(SceneIndexes.MAIN_MENU);
        }
    }
}