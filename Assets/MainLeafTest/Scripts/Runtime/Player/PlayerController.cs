﻿using MainLeafTest.GameManagement;
using UnityEngine;
using MainLeafTest.InputManagement;

namespace MainLeafTest.PlayerManagement
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Player _player = default;
        
        private InputManager _inputManager;

        private void Awake()
        {
            InitializeVariables();
            SubscribeToEvents();
        }
        
        private void InitializeVariables()
        {
            _inputManager = InputManager.Instance;
            
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void SubscribeToEvents()
        {
            _inputManager.OnInputUpdate += ReceiveInputs;
        }

        private void ReceiveInputs(InputData inputData)
        {
            if (GameManager.IsPaused)
            {
                return;
            }
            
            _player.PlayerMovementSystem.SetMovement(inputData.PlayerMovement);
            _player.PlayerMovementSystem.SetJumpInput(inputData.JumpInput);
            _player.PlayerCamera.SetMouseDelta(inputData.PlayerLook);
            _player.PlayerBowSystem.SetShootInput(inputData.ShootInput);
            _player.PlayerBowSystem.SetPrepareArrowInput(inputData.PrepareArrowInput);
        }
    }
}