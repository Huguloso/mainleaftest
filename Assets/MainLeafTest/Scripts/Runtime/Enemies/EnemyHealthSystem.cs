﻿using System;
using MainLeafTest.HealthManagement;
using MainLeafTest.ObjectPoolerManagement;
using UnityEngine;
using System.Collections;

namespace MainLeafTest.EnemyManagement
{
    public class EnemyHealthSystem : HealthSystem, IPoolableSpawn
    {
        public event Action OnEnemyDamaged;
        public event Action OnEnemyDeath;

        private const int BODY_VANISH_TIME = 5;
        
        public override void TakeDamage(int damage)
        {
            if (IsDead)
            {
                return;
            }
            
            base.TakeDamage(damage);
            OnEnemyDamaged?.Invoke();
            Enemy.Animator.SetTrigger(EnemyAnimationTriggers.GOT_HIT);
        }

        public void OnPoolableSpawn()
        {
            Health = Enemy.EnemyStats.MaxHealth;
            IsDead = false;
        }
        
        protected override void Die()
        {
            base.Die();
            
            OnEnemyDeath?.Invoke();
            
            Enemy.Animator.SetTrigger(EnemyAnimationTriggers.DIED);

            StartCoroutine(Disappear());

            IEnumerator Disappear()
            {
                yield return new WaitForSeconds(BODY_VANISH_TIME);
                gameObject.SetActive(false);
            }
        }
        
        protected void Start()
        {
            OnPoolableSpawn();
        }

        protected Enemy Enemy { get; set; }
    }
}