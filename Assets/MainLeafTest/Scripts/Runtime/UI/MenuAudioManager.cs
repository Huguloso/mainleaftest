﻿using UnityEngine;

namespace MainLeafTest.UIManagement
{
    public class MenuAudioManager : MonoBehaviour
    {
        [SerializeField] private AudioClip _clickSound;
        [SerializeField] private AudioSource _audioSource;

        public void PlayClickSound()
        {
            _audioSource.PlayOneShot(_clickSound);
        }
    }
}