﻿using UnityEngine;

namespace MainLeafTest.CameraShakeManagement
{
    [CreateAssetMenu(fileName = "New CameraShakeEventData", menuName = "CameraShake Event Data")]
    public class CameraShakeEventData : ScriptableObject
    {
        [SerializeField] private float _amplitude = 3.0f;
        [SerializeField] private float _frequency = 1.0f;

        [SerializeField] private float _duration = 0.75f;

        [SerializeField] private AnimationCurve _blendOverLifetime = new AnimationCurve(

            new Keyframe(0.0f, 0.0f, Mathf.Deg2Rad * 0.0f, Mathf.Deg2Rad * 720.0f),
            new Keyframe(0.2f, 1.0f),
            new Keyframe(1.0f, 0.0f)
        );

        public void Init(float amplitude, float frequency, float duration,
            AnimationCurve blendOverLifetime)
        {

            Amplitude = amplitude;
            Frequency = frequency;

            Duration = duration;

            BlendOverLifetime = blendOverLifetime;
        }

        public float Amplitude
        {
            get => _amplitude;
            private set => _amplitude = value;
        }

        public float Frequency
        {
            get => _frequency;
            private set => _frequency = value;
        }

        public float Duration
        {
            get => _duration;
            private set => _duration = value;
        }

        public AnimationCurve BlendOverLifetime
        {
            get => _blendOverLifetime;
            private set => _blendOverLifetime = value;
        }
    }
}