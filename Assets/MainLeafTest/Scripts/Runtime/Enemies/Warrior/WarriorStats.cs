﻿using UnityEngine;

namespace MainLeafTest.EnemyManagement.WarriorManagement
{
    [CreateAssetMenu (fileName = "New WarriorStats", menuName = "Enemy Stats/Warrior Stats")]
    public class WarriorStats : EnemyStats
    {
        [SerializeField] private int _movementSpeed = 8;
        [SerializeField] private int _attackMovementSpeed = 2;
        [SerializeField] private int _staggeredMovementSpeed = 0;
        
        [SerializeField] private float _attackArea = 1.2f;
        [SerializeField] private float _rangeToAttack = 2.5f;

        public int MovementSpeed => _movementSpeed;
        public int AttackMovementSpeed => _attackMovementSpeed;
        public int StaggeredMovementSpeed => _staggeredMovementSpeed;

        public float AttackArea => _attackArea;
        public float RangeToAttack => _rangeToAttack;
    }
}