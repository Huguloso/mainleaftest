﻿using UnityEngine;
using System;

namespace MainLeafTest.ObjectPoolerManagement
{
    [Serializable]
    public struct EnemyPool
    {
        public EnemyPoolNames PoolName;
        public int Size;
        public GameObject Prefab;
    }
}