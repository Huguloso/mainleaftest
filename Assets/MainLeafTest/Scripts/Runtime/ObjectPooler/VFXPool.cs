﻿using UnityEngine;
using System;

namespace MainLeafTest.ObjectPoolerManagement
{
    [Serializable]
    public struct VFXPool
    {
        public VFXPoolNames PoolName;
        public int Size;
        public GameObject Prefab;
    }
}