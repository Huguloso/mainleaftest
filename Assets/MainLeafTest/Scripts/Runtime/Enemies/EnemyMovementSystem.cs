﻿using UnityEngine;

namespace MainLeafTest.EnemyManagement
{
    public class EnemyMovementSystem : MonoBehaviour
    {
        private float _verticalVelocity;
        
        protected virtual void Update()
        {
            if (!Enemy.CharacterController.enabled)
            {
                return;
            }
            Move();
        }
        
        protected virtual void Move()
        {
            CalculateGravity();

            Vector3 movement = Vector3.zero;
            movement.y = _verticalVelocity;

            Enemy.CharacterController.Move(movement * Time.deltaTime);
        }

        protected virtual void CalculateGravity()
        {
            if (!Enemy.CharacterController.isGrounded)
            {
                _verticalVelocity += Enemy.EnemyStats.Gravity * Time.deltaTime;
            }
            else
            {
                _verticalVelocity = -1;
            }
        }
        
        protected Enemy Enemy { get; set; }
    }
}