﻿using System;
using MainLeafTest.GameManagement;
using MainLeafTest.ObjectPoolerManagement;
using UnityEngine;

namespace MainLeafTest.EnemyManagement.SpawnManagement
{
    public class EnemyWaveManager : MonoBehaviour
    {
        public event Action OnArcherKilled;
        public event Action OnWarriorKilled;
        
        [SerializeField] private EnemySpawner _enemySpawner = default;
        [SerializeField] private GameManager _gameManager = default;
        
        [SerializeField] private float _maxEnemySpawnTime = 10;
        [SerializeField] private float _minEnemySpawnTime = 5;
        [SerializeField] private int _maxWarriorsAlive = 5;
        [SerializeField] private int _maxArchersAlive = 3;
        
        private int _warriorsAlive;
        private int _archersAlive;

        private float _timeToNextEnemy;

        public void RemoveEnemy(Enemy deadEnemy)
        {
            if (deadEnemy.EnemyType == EnemyPoolNames.ARCHER)
            {
                _archersAlive--;
                OnArcherKilled?.Invoke();
            }
            else
            {
                _warriorsAlive--;
                OnWarriorKilled?.Invoke();
            }
        }

        private void Awake()
        {
            InitializeVariables();
        }

        private void Update()
        {
            if (!_gameManager.MatchStarted || !CheckIfCanSpawn())
            {
                return;
            }
            
            RequestEnemySpawn();

            SetNewTimeToNextEnemy();
        }
        
        private void InitializeVariables()
        {
            _warriorsAlive = 0;
            _archersAlive = 0;
            _timeToNextEnemy = UnityEngine.Random.Range(_minEnemySpawnTime, _maxEnemySpawnTime);
        }

        private bool CheckIfCanSpawn()
        {
            if (_timeToNextEnemy <= 0)
            {
                return true;
            }
            
            _timeToNextEnemy -= Time.deltaTime;
            return false;
        }

        private void SetNewTimeToNextEnemy()
        {
            _timeToNextEnemy = UnityEngine.Random.Range(_minEnemySpawnTime, _maxEnemySpawnTime);
        }   
        
        private void RequestEnemySpawn()
        {
            if (_warriorsAlive >= _maxWarriorsAlive && _archersAlive >= _maxArchersAlive)
            {
                return;
            }

            if (_warriorsAlive >= _maxWarriorsAlive)
            {
                _enemySpawner.SpawnEnemy(EnemyPoolNames.ARCHER);
                _archersAlive++;
                return;
            }
            
            if(_archersAlive >= _maxArchersAlive)
            {
                _enemySpawner.SpawnEnemy(EnemyPoolNames.WARRIOR);
                _warriorsAlive++;
                return;
            }

            int enemyToSpawn = UnityEngine.Random.Range(0, 100);
            if (enemyToSpawn < 50)
            {
                _enemySpawner.SpawnEnemy(EnemyPoolNames.ARCHER);
                _archersAlive++;
            }
            else
            {
                _enemySpawner.SpawnEnemy(EnemyPoolNames.WARRIOR);
                _warriorsAlive++;
            }
        }
    }
}