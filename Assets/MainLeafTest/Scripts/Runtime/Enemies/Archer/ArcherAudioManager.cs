﻿using UnityEngine;

namespace MainLeafTest.EnemyManagement.ArcherManagement
{
    public class ArcherAudioManager : EnemyAudioManager
    {
        [SerializeField] private Archer _archer = default;

        [SerializeField] private AudioClip[] _arrowShotSounds =default;

        protected override void Start()
        {
            base.Start();
            SubscribeToEvents();
        }

        protected override void SubscribeToEvents()
        {
            base.SubscribeToEvents();
            _archer.ArcherAttackSystem.OnArrowFired += PlayArrowShotSound;
        }

        private void Awake()
        {
            InitializeVariables();
        }

        private void InitializeVariables()
        {
            Enemy = _archer;
        }
        
        private void PlayArrowShotSound()
        {
            if (_arrowShotSounds.Length <= 0)
            {
                return;
            }
            
            int soundClip = Random.Range(0, _arrowShotSounds.Length);
            AudioSource audioSource = SpawnAudioSource();
            audioSource.PlayOneShot(_arrowShotSounds[soundClip]);
        }
    }
}
