﻿using MainLeafTest.ObjectPoolerManagement;
using UnityEngine;

namespace MainLeafTest.PlayerManagement
{
    public class PlayerAudioManager : MonoBehaviour
    {
        [SerializeField] private Player _player = default;
        [SerializeField] private AudioSource _footstepsSource;
        [SerializeField] private AudioSource _prepareBowAudioSource;
        
        [SerializeField] private AudioClip _runningFootstepsSound = default;
        [SerializeField] private AudioClip _walkingFootstepsSound = default;
        [SerializeField] private AudioClip[] _gotHitSounds = default;
        [SerializeField] private AudioClip[] _firedArrowSounds = default;
        [SerializeField] private AudioClip[] _preparedArrowSounds = default;
        [SerializeField] private AudioClip[] _reloadSounds = default;
        [SerializeField] private AudioClip[] _deathSounds = default;
        [SerializeField] private AudioClip[] _jumpingSounds = default;

        private void Start()
        {
            SubscribeToEvents();
        }

        private void SubscribeToEvents()
        {
            _player.PlayerHealthSystem.OnPlayerDamaged += PlayGotHitSound;
            _player.PlayerHealthSystem.OnPlayerDeath += PlayDeathSound;

            _player.PlayerBowSystem.OnArrowReload += PlayReloadSound;
            _player.PlayerBowSystem.OnArrowPrepare += PlayPrepareArrowSound;
            _player.PlayerBowSystem.OnArrowShot += ()=>
            {
                PlayFireArrowSound();
                StopPrepareArrowSound();
            };

            _player.PlayerMovementSystem.OnPlayerJumped += PlayJumpingSound;
            
            _player.PlayerMovementSystem.OnPlayerRunning += PlayRunningFootstepSound;
            _player.PlayerMovementSystem.OnPlayerWalking += PlayWalkingFootstepSound;

            _player.PlayerMovementSystem.OnPlayerNotMovingOnGround += StopFootstepsSound;
        }

        private void PlayGotHitSound()
        {
            if (_gotHitSounds.Length <= 0)
            {
                return;
            }
            
            int soundClip = Random.Range(0, _gotHitSounds.Length);
            AudioSource audioSource = SpawnAudioSource();
            audioSource.PlayOneShot(_gotHitSounds[soundClip]);
        }

        private void PlayFireArrowSound()
        {
            if (_firedArrowSounds.Length <= 0)
            {
                return;
            }
            
            int soundClip = Random.Range(0, _firedArrowSounds.Length);
            AudioSource audioSource = SpawnAudioSource();
            audioSource.PlayOneShot(_firedArrowSounds[soundClip]);
        }

        private void PlayDeathSound()
        {
            if (_deathSounds.Length <= 0)
            {
                return;
            }

            int soundClip = Random.Range(0, _deathSounds.Length);
            AudioSource audioSource = SpawnAudioSource();
            audioSource.PlayOneShot(_deathSounds[soundClip]);
        }

        private void PlayReloadSound()
        {
            if (_reloadSounds.Length <= 0)
            {
                return;
            }

            int soundClip = Random.Range(0, _reloadSounds.Length);
            AudioSource audioSource = SpawnAudioSource();
            audioSource.PlayOneShot(_reloadSounds[soundClip]);
        }
        private void PlayPrepareArrowSound()
        {
            if (_preparedArrowSounds.Length <= 0)
            {
                return;
            }

            int soundClip = Random.Range(0, _preparedArrowSounds.Length);
            _prepareBowAudioSource.PlayOneShot(_preparedArrowSounds[soundClip]);
        }

        private void StopPrepareArrowSound()
        {
            if (_prepareBowAudioSource.isPlaying)
            {
                _prepareBowAudioSource.Stop();
            }
        }
        
        private void PlayJumpingSound()
        {
            if (_jumpingSounds.Length <= 0)
            {
                return;
            }

            int soundClip = Random.Range(0, _jumpingSounds.Length);
            AudioSource audioSource = SpawnAudioSource();
            audioSource.PlayOneShot(_jumpingSounds[soundClip]);
        }

        private void PlayRunningFootstepSound()
        {
            _footstepsSource.clip = _runningFootstepsSound;
            
            if (!_footstepsSource.isPlaying)
            {
                _footstepsSource.Play();
            }
        }
        
        private void PlayWalkingFootstepSound()
        {
            _footstepsSource.clip = _walkingFootstepsSound;
            
            if (!_footstepsSource.isPlaying)
            {
                _footstepsSource.Play();
            }
        }
        
        private void StopFootstepsSound()
        {
            if (_footstepsSource.isPlaying)
            {
                _footstepsSource.Pause();
            }
        }
        
        private AudioSource SpawnAudioSource()
        {
            return _player.ObjectPooler.SpawnFromAudioPool(AudioPoolNames.DEFAULT_AUDIO_SOURCE,
                Vector3.zero, Quaternion.identity, transform);
        }
    }
}