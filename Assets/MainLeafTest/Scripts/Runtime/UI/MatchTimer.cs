﻿using MainLeafTest.GameManagement;
using TMPro;
using UnityEngine;

namespace MainLeafTest.UIManagement
{
    public class MatchTimer : MonoBehaviour
    {
        [SerializeField] private TMP_Text _timeText = default;
        [SerializeField] private GameManager _gameManager = default;

        private void Update()
        {
            WriteTime();
        }

        private void WriteTime()
        {
            if (_gameManager.MatchStarted)
            {
                int minutes = (int) _gameManager.MatchTimeLeft / 60;
                int seconds = (int) _gameManager.MatchTimeLeft % 60;
                _timeText.text = minutes + ":" + seconds;
            }
            else
            {
                int seconds = (int) _gameManager.TimeToStartMatch;
                _timeText.text = seconds.ToString();
            }
        }
    }
}