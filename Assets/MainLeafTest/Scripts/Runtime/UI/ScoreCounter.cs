﻿using MainLeafTest.GameManagement.ScoreManagement;
using TMPro;
using UnityEngine;

namespace MainLeafTest.UIManagement
{
    public class ScoreCounter : MonoBehaviour
    {
        [SerializeField] private ScoreManager _scoreManager = default;
        [SerializeField] private TextMeshProUGUI _scoreText = default;
        private void Start()
        {
            UpdateScoreText();
            SubscribeToEvents();
        }

        private void UpdateScoreText()
        {
            _scoreText.text = _scoreManager.Points.ToString();
        }

        private void SubscribeToEvents()
        {
            _scoreManager.OnScoreUpdated += UpdateScoreText;
        }
    }
}