﻿namespace MainLeafTest.ObjectPoolerManagement
{
    public interface IPoolableSpawn
    {
        void OnPoolableSpawn();
    }
}
