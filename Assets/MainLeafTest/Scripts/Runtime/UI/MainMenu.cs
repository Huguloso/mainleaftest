﻿using MainLeafTest.GameManagement;
using UnityEngine;
using UnityEngine.UI;

namespace MainLeafTest.UIManagement
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] private LevelLoader _levelLoader = default;
        
        [SerializeField] private GameObject _optionsMenu = default;
        [SerializeField] private GameObject _controlsScreen = default;
        
        [SerializeField] private Button _playButton = default;
        [SerializeField] private Button _controlsButton = default;
        [SerializeField] private Button _optionsButton = default;
        [SerializeField] private Button _quitButton = default;
    
        [SerializeField] private MenuAudioManager _audioManager = default;
        
        private void Start()
        {
            SubscribeToEvents();
        }

        private void SubscribeToEvents()
        {
            _playButton.onClick.AddListener(() =>
            {
                Play();
                _audioManager.PlayClickSound();
            });
            
            _controlsButton.onClick.AddListener(() =>
            {
                OpenControlScreen();
                _audioManager.PlayClickSound();
            });
            
            _optionsButton.onClick.AddListener(() =>
            {
                OpenOptions();
                _audioManager.PlayClickSound();
            });
            
            _quitButton.onClick.AddListener(() =>
            {
                Quit();
                _audioManager.PlayClickSound();
            });
        }

        private void Play()
        {
            _levelLoader.LoadLevel(SceneIndexes.CEMETERY);
            gameObject.SetActive(false);
        }

        private void OpenControlScreen()
        {
            _controlsScreen.SetActive(true);
            gameObject.SetActive(false);
        }
        
        private void OpenOptions()
        {
            _optionsMenu.SetActive(true);
            gameObject.SetActive(false);
        }

        private void Quit()
        {
            Application.Quit();
        }
    }
}