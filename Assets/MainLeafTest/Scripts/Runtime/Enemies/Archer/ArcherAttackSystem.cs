﻿using System;
using MainLeafTest.ArrowManagement;
using MainLeafTest.GameManagement.CollisionManagement;
using MainLeafTest.EnemyManagement;
using MainLeafTest.EnemyManagement.ArcherManagement;
using MainLeafTest.ObjectPoolerManagement;
using UnityEngine;

namespace MainLeafTest.EnemyManagement.ArcherManagement
{
    public class ArcherAttackSystem : EnemyAttackSystem
    {
        public event Action OnArrowFired;

        [SerializeField] private Archer _archer = default;
        [SerializeField] private Transform _lineOfSightStart = default;
        [SerializeField] private Transform _arrowSpawnPoint = default;

        protected override void InitializeVariables()
        {
            base.InitializeVariables();
            Enemy = _archer;
        }

        private void Awake()
        {
            InitializeVariables();
        }

        private void Update()
        {
            if (_archer.ArcherHealthSystem.IsDead)
            {
                return;
            }

            CheckForLineOfSight();
            FacePlayer();
        }

        private void CheckForLineOfSight()
        {
            if (!CanAttack)
            {
                return;
            }

            RaycastHit hit;
            Vector3 direction = _archer.Player.transform.position - _lineOfSightStart.position;

            if (!Physics.Raycast(_lineOfSightStart.position, direction, out hit))
            {
                return;
            }

            if (hit.collider.gameObject.layer == (int) CollisionLayers.PLAYER)
            {
                Attack();
            }
        }

        private void ShootArrow()
        {
            Arrow arrow = _archer.ObjectPooler.SpawnArrowFromPool(ArrowPoolNames.ENEMY_ARROW, Vector3.zero,
                Quaternion.Euler(-90, 0, 0), _arrowSpawnPoint);

            arrow.Fire(_archer.ArcherStats.BowForce, _archer.ArcherStats.Damage);
            OnArrowFired?.Invoke();
        }

        private void FacePlayer()
        {
            transform.LookAt(_archer.Player.transform);
            transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);

            _arrowSpawnPoint.LookAt(_archer.Player.transform);
        }
    }
}