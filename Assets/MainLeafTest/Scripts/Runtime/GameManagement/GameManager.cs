﻿using System;
using System.Collections;
using UnityEngine;
using MainLeafTest.PlayerManagement;

namespace MainLeafTest.GameManagement
{
    public class GameManager : MonoBehaviour
    {
        public event Action OnMatchEnded;
        public event Action OnPlayerDefeated;
        
        [SerializeField] private Player _player = default;

        private bool _matchEnded;

        private void Awake()
        {
            InitializeVariables();
        }

        private void Start()
        {
            SubscribeToEvents();
        }

        private void Update()
        {
            DecreaseMatchTime();
            CheckIfMatchEnded();
        }
        
        private void InitializeVariables()
        {
            Time.timeScale = 1;
            TimeToStartMatch = PlayerPrefs.GetFloat(PlayerPrefsKeys.INITIAL_COUNTDOWN, 3);
            MatchTimeLeft = PlayerPrefs.GetInt(PlayerPrefsKeys.MATCH_TIME, 180);
            IsPaused = false;
        }

        private void SubscribeToEvents()
        {
            _player.PlayerHealthSystem.OnPlayerDeath += () =>
            {
                OnPlayerDefeated?.Invoke();
            };
        }
        
        private void DecreaseMatchTime()
        {
            if (!MatchStarted)
            {
                TimeToStartMatch -= Time.deltaTime;
                if (TimeToStartMatch <= 0)
                {
                    MatchStarted = true;
                }
                return;
            }
            
            if (MatchTimeLeft < 0 || IsPaused)
            {
                return;
            }
            
            MatchTimeLeft -= Time.deltaTime;
        }

        private void CheckIfMatchEnded()
        {
            if (_matchEnded || MatchTimeLeft > 0)
            {
                return;
            }
            
            _matchEnded = true;
            OnMatchEnded?.Invoke();
        }

        public static bool IsPaused { get; set; }
        
        public float TimeToStartMatch { get; private set; }
        public bool MatchStarted { get; private set; }
        public float MatchTimeLeft { get; private set; }
    }
}