﻿using MainLeafTest.ObjectPoolerManagement;
using MainLeafTest.PlayerManagement.Camera;
using UnityEngine;

namespace MainLeafTest.PlayerManagement
{
    public class Player : MonoBehaviour
    {
        [SerializeField] private PlayerStats _playerStats = default;
        [SerializeField] private PlayerMovementSystem _playerMovementSystem = default;
        [SerializeField] private PlayerHealthSystem _playerHealthSystem;
        [SerializeField] private CinemachinePOVExtension _playerCamera = default;
        [SerializeField] private PlayerBowSystem _playerBowSystem = default;
        
        [SerializeField] private CharacterController _characterController = default;
        [SerializeField] private Animator _animator = default;

        private ObjectPooler _objectPooler;

        private void Start()
        {
            InitializeVariables();
        }
        
        private void InitializeVariables()
        {
            _objectPooler = ObjectPooler.Instance;
        }
        
        public PlayerStats PlayerStats => _playerStats;
        public PlayerMovementSystem PlayerMovementSystem => _playerMovementSystem;
        public PlayerHealthSystem PlayerHealthSystem => _playerHealthSystem;
        public CinemachinePOVExtension PlayerCamera => _playerCamera;
        public PlayerBowSystem PlayerBowSystem => _playerBowSystem;
        public CharacterController CharacterController => _characterController;
        public Animator Animator => _animator;
        public ObjectPooler ObjectPooler => _objectPooler;
    }
}