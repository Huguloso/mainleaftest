﻿using System;
using UnityEngine;

namespace MainLeafTest.ObjectPoolerManagement
{
    [Serializable]
    public struct ArrowPool 
    {
        public ArrowPoolNames PoolName;
        public int Size;
        public GameObject Prefab;
    }
}