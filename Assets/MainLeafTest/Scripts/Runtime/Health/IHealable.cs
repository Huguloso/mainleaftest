﻿namespace MainLeafTest.HealthManagement
{
    public interface IHealable
    {
        bool Heal(int healAmount);
    }
}