﻿using MainLeafTest.GameManagement;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using Button = UnityEngine.UI.Button;
using Slider = UnityEngine.UI.Slider;

namespace MainLeafTest.UIManagement
{
    public class OptionsScreen : MonoBehaviour
    {
        [SerializeField] private GameObject _previousScreen = default;

        [SerializeField] private Button _backButton = default;
        
        [SerializeField] private Slider _sfxSlider = default;
        [SerializeField] private Slider _musicSlider = default;
        [SerializeField] private Slider _initialCountdownSlider = default;
        [SerializeField] private Slider _matchTimeSlider = default;

        [SerializeField] private TextMeshProUGUI _sfxValue = default;
        [SerializeField] private TextMeshProUGUI _musicValue = default;
        [SerializeField] private TextMeshProUGUI _initialCountdownValue = default;
        [SerializeField] private TextMeshProUGUI _matcTimeValue = default;
        
        [SerializeField] private AudioMixer _audioMixer = default;
        [SerializeField] private MenuAudioManager _audioManager = default;
        
        private void Start()
        {
            SubscribeToEvents();
        }

        private void OnEnable()
        {
            InitializeSliders();
        }

        private void InitializeSliders()
        {
            
            float value = PlayerPrefs.GetFloat(PlayerPrefsKeys.SFX_VOLUME, 0);
            _audioMixer.SetFloat("SFXVolume", value);
            
            value /= 20;

            value = Mathf.Pow(10, value);
                
            _sfxSlider.value = value;
            _sfxValue.text = ((int)(value * 100)).ToString();
            
            value = PlayerPrefs.GetFloat(PlayerPrefsKeys.MUSIC_VOLUME, 0);
            _audioMixer.SetFloat("MusicVolume", value);
            
            value /= 20;
                
            value = Mathf.Pow(10, value);

            _musicSlider.value = value;
            _musicValue.text = ((int)(value * 100)).ToString();

            if (_initialCountdownSlider != null)
            {
                _initialCountdownSlider.value = PlayerPrefs.GetFloat(PlayerPrefsKeys.INITIAL_COUNTDOWN, 3);
                _initialCountdownValue.text = _initialCountdownSlider.value.ToString("F1");
            }

            if (_matchTimeSlider != null)
            {
                _matchTimeSlider.value = PlayerPrefs.GetInt(PlayerPrefsKeys.MATCH_TIME, 180);
                _matcTimeValue.text = _matchTimeSlider.value.ToString();
            }
        }
        
        private void SubscribeToEvents()
        {
            _backButton.onClick.AddListener(() =>
            {
                BackToPreviousScreen();
                _audioManager.PlayClickSound();
            });
            
            _sfxSlider.onValueChanged.AddListener(SetSfxVolume);
            _musicSlider.onValueChanged.AddListener(SetMusicVolume);

            if (_matchTimeSlider != null)
            {
                _matchTimeSlider.onValueChanged.AddListener(SetMatchTime);
            }

            if (_initialCountdownSlider != null)
            {
                _initialCountdownSlider.onValueChanged.AddListener(SetInitialCountdown);
            }
        }

        private void BackToPreviousScreen()
        {
            _previousScreen.SetActive(true);
            gameObject.SetActive(false);
        }

        private void SetSfxVolume(float volume)
        {
            _audioMixer.SetFloat("SFXVolume", Mathf.Log10(volume) * 20);
            PlayerPrefs.SetFloat(PlayerPrefsKeys.SFX_VOLUME, Mathf.Log10(volume) * 20);
            
            _sfxValue.text = ((int)(volume * 100)).ToString();
        }
        
        private void SetMusicVolume(float volume)
        {
            _audioMixer.SetFloat("MusicVolume", Mathf.Log10(volume) * 20);
            PlayerPrefs.SetFloat(PlayerPrefsKeys.MUSIC_VOLUME, Mathf.Log10(volume) * 20);
            
            _musicValue.text = ((int)(volume * 100)).ToString();
        }

        private void SetMatchTime(float value)
        {
            PlayerPrefs.SetInt(PlayerPrefsKeys.MATCH_TIME, (int)value);
            _matcTimeValue.text = _matchTimeSlider.value.ToString();
        }

        private void SetInitialCountdown(float value)
        {
            PlayerPrefs.SetFloat(PlayerPrefsKeys.INITIAL_COUNTDOWN, value);
            _initialCountdownValue.text = value.ToString("F1");
        }
    }
}