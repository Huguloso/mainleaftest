﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MainLeafTest.PlayerManagement
{
    public class PlayerUIManager : MonoBehaviour
    {
        [SerializeField] private Player _player = default;

        [SerializeField] private Slider _healthBar = default;
        [SerializeField] private TextMeshProUGUI _arrowCounter = default;
        [SerializeField] private Animation _damageBlur = default;
        
        private void Start()
        {
            InitializeUIVariables();
            SubscribeToEvents();
        }

        private void InitializeUIVariables()
        {
            _healthBar.maxValue = _player.PlayerStats.MaxHealth;
            UpdateHealthBar();
            UpdateArrowCounter();
        }

        private void SubscribeToEvents()
        {
            _player.PlayerHealthSystem.OnPlayerDamaged += (() =>
            {
                UpdateHealthBar();
                PlayDamageBlurAnimation();
            });
            
            _player.PlayerHealthSystem.OnPlayerHealed += UpdateHealthBar;
            _player.PlayerBowSystem.OnArrowShot += UpdateArrowCounter;
            _player.PlayerBowSystem.OnArrowCollected += UpdateArrowCounter;
        }

        private void PlayDamageBlurAnimation()
        {
            _damageBlur.Play();
        }
        
        private void UpdateHealthBar()
        {
            _healthBar.value = _player.PlayerHealthSystem.Health;
        }

        private void UpdateArrowCounter()
        {
            _arrowCounter.text = _player.PlayerBowSystem.Arrows.ToString();
        }
    }
}