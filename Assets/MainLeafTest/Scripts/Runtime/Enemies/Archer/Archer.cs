﻿using MainLeafTest.ObjectPoolerManagement;
using UnityEngine;

namespace MainLeafTest.EnemyManagement.ArcherManagement
{
    public class Archer : Enemy
    {
        [SerializeField] private ArcherStats _archerStats = default;
        [SerializeField] private ArcherAttackSystem _archerAttackSystem = default;
        [SerializeField] private ArcherHealthSystem _archerHealthSystem = default;
        [SerializeField] private ArcherMovementSystem _archerMovementSystem = default;

        private void Awake()
        {
            InitializeEnemyVariables();
        }

        private void InitializeEnemyVariables()
        {
            EnemyType = EnemyPoolNames.ARCHER;
            
            EnemyStats = ArcherStats;
            EnemyHealthSystem = ArcherHealthSystem;
            EnemyMovementSystem = ArcherMovementSystem;
            EnemyAttackSystem = ArcherAttackSystem;
        }
        
        public ArcherStats ArcherStats => _archerStats;
        public ArcherAttackSystem ArcherAttackSystem => _archerAttackSystem;
        public ArcherHealthSystem ArcherHealthSystem => _archerHealthSystem;
        public ArcherMovementSystem ArcherMovementSystem => _archerMovementSystem;
    }
}