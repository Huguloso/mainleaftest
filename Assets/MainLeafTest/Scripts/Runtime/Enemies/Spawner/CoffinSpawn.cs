﻿using MainLeafTest.EnemyManagement.ArcherManagement;
using UnityEngine;

namespace MainLeafTest.EnemyManagement.SpawnManagement
{
    public class CoffinSpawn : MonoBehaviour
    {
        private const string SPAWN_WARRIOR_ANIMATION_TRIGGER = "SpawnWarrior";
        private const string SPAWN_ARCHER_ANIMATION_TRIGGER = "SpawnArcher";
        
        [SerializeField] private Transform _spawnPoint = default;
        [SerializeField] private Animator _animator = default;
        [SerializeField] private SkinnedMeshRenderer _warriorArmor = default;
        [SerializeField] private SkinnedMeshRenderer _archerArmor = default;
        [SerializeField] private AudioSource _audioSource = default;
        
        private Archer _archer;

        public void PlayArcherAnimation(Material armorColor)
        {
            _archerArmor.material = armorColor;
            _animator.SetTrigger(SPAWN_ARCHER_ANIMATION_TRIGGER);
            PlaySound();
        }

        public void PlayWarriorAnimation(Material armorColor)
        {
            _warriorArmor.material = armorColor;
            _animator.SetTrigger(SPAWN_WARRIOR_ANIMATION_TRIGGER);
            PlaySound();
        }

        public void OccupyPointWithArcher(Archer archer)
        {
            _archer = archer;
            OccupiedByArcher = true;
            SubscribeToArcherDeath();
        }

        private void PlaySound()
        {
            _audioSource.Play();    
        }
        
        private void SubscribeToArcherDeath()
        {
            _archer.ArcherHealthSystem.OnEnemyDeath += FreePoint;
        }

        private void FreePoint()
        {
            OccupiedByArcher = false;
            _archer.ArcherHealthSystem.OnEnemyDeath -= FreePoint;
        }
        
        public Transform SpawnPoint => _spawnPoint;

        public bool OccupiedByArcher { get; private set; }
    }
}