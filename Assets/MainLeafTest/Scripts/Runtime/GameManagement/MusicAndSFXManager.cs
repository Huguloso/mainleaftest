﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

namespace MainLeafTest.GameManagement.MusicManagement
{
    public class MusicAndSFXManager : MonoBehaviour
    {
        [SerializeField] private bool _delayMusic = false;
        
        [SerializeField] private AudioSource _audioSource = default;

        [SerializeField] private AudioMixer _audioMixer = default;
        
        private void Start()
        {
            InitializeVolumes();
            StartCoroutine(StartMusicCoroutine());
        }

        private void InitializeVolumes()
        {
            float value = PlayerPrefs.GetFloat(PlayerPrefsKeys.SFX_VOLUME, 0);
            _audioMixer.SetFloat("SFXVolume", value);
            
            value = PlayerPrefs.GetFloat(PlayerPrefsKeys.MUSIC_VOLUME, 0);
            _audioMixer.SetFloat("MusicVolume", value);
        }

        private IEnumerator StartMusicCoroutine()
        {
            float time = 0;

            if (_delayMusic)
            {
                time = PlayerPrefs.GetFloat(PlayerPrefsKeys.INITIAL_COUNTDOWN, 3);
            }
            
            yield return new WaitForSeconds(time);
            _audioSource.Play();
        }
    }
}