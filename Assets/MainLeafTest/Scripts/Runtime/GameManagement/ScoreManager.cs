﻿using System;
using MainLeafTest.EnemyManagement;
using MainLeafTest.EnemyManagement.SpawnManagement;
using UnityEngine;

namespace MainLeafTest.GameManagement.ScoreManagement
{
    public class ScoreManager : MonoBehaviour
    {
        public event Action OnScoreUpdated;
        [SerializeField] private EnemyWaveManager _waveManager = default;
        [SerializeField] private int _pointsPerArcher = 100;
        [SerializeField] private int _pointsPerWarrior = 200;

        private void Start()
        {
            SubscribeToEvents();
        }

        private void SubscribeToEvents()
        {
            _waveManager.OnArcherKilled += () =>
            {
                Points += _pointsPerArcher;
                EnemiesKilled++;
                OnScoreUpdated?.Invoke();
            };
            _waveManager.OnWarriorKilled += () =>
            {
                Points += _pointsPerWarrior;
                EnemiesKilled++;
                OnScoreUpdated.Invoke();
            };
        }

        public int Points { get; private set; }

        public int EnemiesKilled { get; private set; }
    }
}