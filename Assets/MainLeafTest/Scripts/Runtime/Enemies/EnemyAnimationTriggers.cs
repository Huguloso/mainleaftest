﻿namespace MainLeafTest.EnemyManagement
{
    public struct EnemyAnimationTriggers
    {
        public const string ATTACK = "Attack";
        public const string GOT_HIT = "GotHit";
        public const string DIED = "Died";
    }
}