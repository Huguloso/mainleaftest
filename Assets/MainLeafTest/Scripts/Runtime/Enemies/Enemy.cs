﻿using MainLeafTest.EnemyManagement.SpawnManagement;
using MainLeafTest.ObjectPoolerManagement;
using UnityEngine;
using MainLeafTest.PlayerManagement;

namespace MainLeafTest.EnemyManagement
{
    public class Enemy : MonoBehaviour, IPoolableSpawn
    {
        [SerializeField] private Animator _animator = default;
        [SerializeField] private CharacterController _characterController = default;
        [SerializeField] private SkinnedMeshRenderer _armor = default;
        
        private Material _armorColor;
        
        public void OnPoolableSpawn()
        {
            EnemyHealthSystem.OnEnemyDeath += DecreaseWaveManagerCount;
        }

        public void SetArmorColor(Material color)
        {
            _armor.material = color;
        }
        
        protected virtual void Start()
        {
            InitializeVariables();
        }

        private void InitializeVariables()
        {
            ObjectPooler = ObjectPooler.Instance;
        }
        
        private void DecreaseWaveManagerCount()
        {
            WaveManager.RemoveEnemy(this);
            EnemyHealthSystem.OnEnemyDeath -= DecreaseWaveManagerCount;
        }

        public Animator Animator => _animator;
        public CharacterController CharacterController => _characterController;
        
        public Player Player { get; set; }
        public ObjectPooler ObjectPooler { get; private set; }
        public EnemyStats EnemyStats { get; protected set; }
        public EnemyHealthSystem EnemyHealthSystem { get; protected set; }
        public EnemyMovementSystem EnemyMovementSystem { get; protected set; }
        public EnemyAttackSystem EnemyAttackSystem { get; protected set; }
        public EnemyPoolNames EnemyType { get; protected set; }
        public EnemyWaveManager WaveManager { get; set; }
    }
}