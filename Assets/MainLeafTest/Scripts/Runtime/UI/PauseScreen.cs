﻿using System;
using UnityEngine;
using MainLeafTest.GameManagement;
using UnityEngine.UI;

namespace MainLeafTest.UIManagement
{
    public class PauseScreen : MonoBehaviour
    {
        public event Action OnResumeButtonClicked;
        
        [SerializeField] private SceneIndexes _currentScene = SceneIndexes.CEMETERY;
        [SerializeField] private LevelLoader _levelLoader = default;
        [SerializeField] private GameObject _optionsScreen = default;
        [SerializeField] private MenuAudioManager _audioManager = default;
        
        [SerializeField] private Button _resumeButton = default;
        [SerializeField] private Button _optionsButton = default;
        [SerializeField] private Button _restartButton = default;
        [SerializeField] private Button _exitButton = default;
        
        private void Start()
        {
            SubscribeToEvents();
        }

        private void SubscribeToEvents()
        {
            _resumeButton.onClick.AddListener(() =>
            {
                Resume();
                _audioManager.PlayClickSound();
            });
            
            _optionsButton.onClick.AddListener(() =>
            {
                OpenOptions();
                _audioManager.PlayClickSound();
            });

            _restartButton.onClick.AddListener(() =>
            {
                Restart();
                _audioManager.PlayClickSound();
            });
            
            _exitButton.onClick.AddListener(() =>
            {
                ExitToMenu();
                _audioManager.PlayClickSound();
            });
        }

        private void Resume()
        {
            OnResumeButtonClicked?.Invoke();
        }

        private void OpenOptions()
        {
            _optionsScreen.SetActive(true);
            gameObject.SetActive(false);
        }
        
        private void Restart()
        {
            Time.timeScale = 1;
            _levelLoader.LoadLevel(_currentScene);
        }
        
        private void ExitToMenu()
        {
            Time.timeScale = 1;
            _levelLoader.LoadLevel(SceneIndexes.MAIN_MENU);
        }
    }
}
