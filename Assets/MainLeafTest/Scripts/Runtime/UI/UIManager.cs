﻿using MainLeafTest.GameManagement;
using UnityEngine;
using MainLeafTest.InputManagement;

namespace MainLeafTest.UIManagement
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private GameManager _gameManager = default;
        
        [SerializeField] private PauseScreen _pauseScreen = default;
        [SerializeField] private GameObject _inGameUI = default;
        [SerializeField] private EndGameScreen _victoryScreen = default;
        [SerializeField] private EndGameScreen _defeatScreen = default;
        
        private InputManager _inputManager;
        
        private void Start()
        {
            HideCursor();
            InitializeVariables();
            SubscribeToEvents();
        }
        
        private void HideCursor()
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void ShowCursor()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        private void PauseGame()
        {
            Time.timeScale = 0;
            GameManager.IsPaused = true;
        }

        private void UnpauseGame()
        {
            Time.timeScale = 1;
            GameManager.IsPaused = false;
        }
        
        private void InitializeVariables()
        {
            _inputManager = InputManager.Instance;
        }

        private void SubscribeToEvents()
        {
            _inputManager.OnInputUpdate += ReceivePauseInput;
            _pauseScreen.OnResumeButtonClicked += ClosePauseScreen;

            _gameManager.OnMatchEnded += OpenVictoryScreen;
            _gameManager.OnPlayerDefeated += OpenDefeatScreen;
        }
        
        private void ReceivePauseInput(InputData data)
        {
            if (data.PauseInput && !GameManager.IsPaused)
            {
                OpenPauseScreen();
            }
        }
        
        private void OpenPauseScreen()
        {
            PauseGame();
            
            _inGameUI.SetActive(false);
            _pauseScreen.gameObject.SetActive(true);

            ShowCursor();
        }

        private void ClosePauseScreen()
        {
            UnpauseGame();
            
            _inGameUI.SetActive(true);
            _pauseScreen.gameObject.SetActive(false);
         
            HideCursor();
        }

        private void OpenVictoryScreen()
        {
            PauseGame();
            
            _inGameUI.SetActive(false);
            _victoryScreen.gameObject.SetActive(true);
            
            ShowCursor();
        }

        private void OpenDefeatScreen()
        {
            PauseGame();
            
            _inGameUI.SetActive(false);
            _defeatScreen.gameObject.SetActive(true);
            
            ShowCursor();
        }
    }
}