﻿using MainLeafTest.ObjectPoolerManagement;
using UnityEngine;

namespace MainLeafTest.EnemyManagement
{
    public class EnemyPickUpDropManager : MonoBehaviour
    {
        private const int BASE_AMMO_SPAWN_CHANCE = 20;
        private const int BASE_HEALTH_SPAWN_CHANCE = 20;

        private const int HIGH_HEALTH_SPAWN_CHANCE = 50;
        private const int HIGH_AMMO_SPAWN_CHANCE = 50;
        
        [SerializeField] private Enemy _enemy = default;

        private int _ammoSpawnChance;
        private int _healthSpawnChance;
        
        private void Awake()
        {
            SubscribeToEvents();
        }

        private void SubscribeToEvents()
        {
            _enemy.EnemyHealthSystem.OnEnemyDeath += RequestPickUpSpawn;
        }

        private void RequestPickUpSpawn()
        {
            int drop = Random.Range(0, 100);

            SetSpawnChances();
            
            if (drop < _ammoSpawnChance)
            {
                _enemy.ObjectPooler.SpawnFromPickUpPool(PickUpPoolNames.ARROW_PICKUP, transform.position,
                    Quaternion.identity);
            }
            else if (drop > _ammoSpawnChance && drop < _ammoSpawnChance + _healthSpawnChance)
            {
                _enemy.ObjectPooler.SpawnFromPickUpPool(PickUpPoolNames.HEALTH_PICKUP, transform.position,
                    Quaternion.identity);
            }
        }

        private void SetSpawnChances()
        {
            _healthSpawnChance = BASE_HEALTH_SPAWN_CHANCE;
            
            if (_enemy.Player.PlayerHealthSystem.LowHealth)
            {
                _healthSpawnChance = HIGH_HEALTH_SPAWN_CHANCE;
            }

            _ammoSpawnChance = BASE_AMMO_SPAWN_CHANCE;
            
            if (_enemy.Player.PlayerBowSystem.LowOnArrows)
            {
                _ammoSpawnChance = HIGH_AMMO_SPAWN_CHANCE;
            }
        }
    }
}