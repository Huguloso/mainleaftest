﻿namespace MainLeafTest.PlayerManagement
{
    public struct PlayerAnimationParameters
    {
        public const string PREPARE_ARROW_TRIGGER = "PrepareArrow";
        public const string RELOAD_ARROW_TRIGGER = "Reload";

        public const string SPEED_INT = "Speed";
    }
}