﻿using UnityEngine.UI;
using UnityEngine;

namespace MainLeafTest.UIManagement
{
    public class ControlsScreen : MonoBehaviour
    {
        [SerializeField] private GameObject _previousScreen = default;
        [SerializeField] private Button _backButton = default;

        [SerializeField] private MenuAudioManager _audioManager = default;

        private void Start()
        {
            SubscribeToEvents();
        }

        private void SubscribeToEvents()
        {
            _backButton.onClick.AddListener(BackToPreviousScreen);
            _backButton.onClick.AddListener(_audioManager.PlayClickSound);
        }

        private void BackToPreviousScreen()
        {
            _previousScreen.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}