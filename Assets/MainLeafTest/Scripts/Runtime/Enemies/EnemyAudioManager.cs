﻿using MainLeafTest.ObjectPoolerManagement;
using UnityEngine;

namespace MainLeafTest.EnemyManagement
{
    public class EnemyAudioManager : MonoBehaviour
    {
        [SerializeField] private AudioClip[] _gotHitSounds = default;
        [SerializeField] private AudioClip[] _deathSounds = default;

        protected virtual void Start()
        {
            SubscribeToEvents();
        }

        protected virtual void SubscribeToEvents()
        {
            Enemy.EnemyHealthSystem.OnEnemyDamaged += PlayGotHitSound;
            Enemy.EnemyHealthSystem.OnEnemyDeath += PlayDeathSound;
        }
        
        protected AudioSource SpawnAudioSource()
        {
            return Enemy.ObjectPooler.SpawnFromAudioPool(AudioPoolNames.DEFAULT_AUDIO_SOURCE,
                Vector3.zero, Quaternion.identity, transform);
        }
        
        private void PlayGotHitSound()
        {
            if (_gotHitSounds.Length <= 0)
            {
                return;
            }
            
            int soundClip = Random.Range(0, _gotHitSounds.Length);
            AudioSource audioSource = SpawnAudioSource();
            audioSource.PlayOneShot(_gotHitSounds[soundClip]);
        }
        
        private void PlayDeathSound()
        {
            if (_deathSounds.Length <= 0)
            {
                return;
            }

            int soundClip = Random.Range(0, _deathSounds.Length);
            AudioSource audioSource = SpawnAudioSource();
            audioSource.PlayOneShot(_deathSounds[soundClip]);
        }

        protected Enemy Enemy { get; set; }
    }
}