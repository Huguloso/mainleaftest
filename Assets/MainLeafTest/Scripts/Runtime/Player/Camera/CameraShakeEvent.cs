﻿using UnityEngine;

namespace MainLeafTest.CameraShakeManagement
{
    public class CameraShakeEvent
    {
        private float _duration;
        private float _timeRemaining;

        private CameraShakeEventData _data;

        public CameraShakeEvent(CameraShakeEventData data)
        {
            _data = data;

            _duration = data.Duration;

            _timeRemaining = _duration;

            Frequency = data.Frequency;
        }

        public void Update()
        {
            _timeRemaining -= Time.deltaTime;
            
            float agePercent = 1.0f - (_timeRemaining / _duration);
            Intensity = _data.Amplitude;
            Intensity *= _data.BlendOverLifetime.Evaluate(agePercent);
        }

        public bool IsAlive()
        {
            return _timeRemaining > 0;
        }

        public float Frequency { get; private set; }

        public float Intensity { get; private set; }
    }
}