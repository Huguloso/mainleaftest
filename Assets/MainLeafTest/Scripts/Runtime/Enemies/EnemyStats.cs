﻿using UnityEngine;

namespace MainLeafTest.EnemyManagement
{
    public class EnemyStats : ScriptableObject
    {
        [SerializeField] private int _maxHealth = 100;
        [SerializeField] private int _damage = 25;
        [SerializeField] private float _gravity = -10;
        [SerializeField] private float _attackRate = 2;
        
        public int MaxHealth => _maxHealth;
        public int Damage => _damage;
        
        public float Gravity => _gravity;
        public float AttackRate => _attackRate;
    }
}