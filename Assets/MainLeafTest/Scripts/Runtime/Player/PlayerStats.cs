﻿using UnityEngine;

namespace MainLeafTest.PlayerManagement
{
    [CreateAssetMenu(fileName = "New PlayerStats", menuName = "PlayerStats")]
    public class PlayerStats : ScriptableObject
    {
        [SerializeField] private int _movementSpeed = 10;
        [SerializeField] private int _aimingMovementSpeed = 5;

        [SerializeField] private int _maxHealth = 100;

        [SerializeField] private int _arrowDamage = 25;
        [SerializeField] private int _arrowHeadshotDamage = 60;
        
        [SerializeField] private float _gravity = -20;
        [SerializeField] private int _jumpForce = 10;
        
        [SerializeField] private float _maxBowChargeTime = 1.0f;
        [SerializeField] private int _maxBowForce = 75;
        [SerializeField] private float _bowReloadTime = 1.0f;

        [SerializeField] private int _maxArrows = 30;
        [SerializeField] private int _startingArrows = 15;

        [SerializeField] private int _lowHealthThreshold = 30;
        [SerializeField] private int _lowArrowsTreshold = 10;
        
        public int MovementSpeed => _movementSpeed;
        public int AimingMovementSpeed => _aimingMovementSpeed;
        
        public int MaxHealth => _maxHealth;
        
        public int ArrowDamage => _arrowDamage;
        public int ArrowHeadshotDamage => _arrowHeadshotDamage;

        public float Gravity => _gravity;
        public int JumpForce => _jumpForce;

        public float MaxBowChargeTime => _maxBowChargeTime;
        public int MaxBowForce => _maxBowForce;
        public float BowReloadTime => _bowReloadTime;

        public int MaxArrows => _maxArrows;
        public int StartingArrows => _startingArrows;

        public int LowHealthThreshold => _lowHealthThreshold;
        public int LowArrowsTreshold => _lowArrowsTreshold;
    }
}