﻿using System;
using UnityEngine;

namespace MainLeafTest.EnemyManagement.WarriorManagement
{
    public class WarriorHealthSystem : EnemyHealthSystem
    {
        public event Action OnGotHitAnimationEnded;
        [SerializeField] private Warrior _warrior = default;

        private void GotHitAnimationEnded()
        {
            OnGotHitAnimationEnded?.Invoke();
        }
        
        private void Awake()
        {
            InitializeVariables();
        }

        private void InitializeVariables()
        {
            Enemy = _warrior;
        }
    }
}