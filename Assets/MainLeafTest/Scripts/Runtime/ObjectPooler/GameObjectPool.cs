﻿using System;
using UnityEngine;

namespace MainLeafTest.ObjectPoolerManagement
{
    [Serializable]
    public struct GameObjectPool
    {
        public GameObjectPoolNames PoolName;
        public int Size;
        public GameObject Prefab;
    }
}