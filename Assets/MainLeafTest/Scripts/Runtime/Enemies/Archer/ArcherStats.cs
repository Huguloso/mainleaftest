﻿using UnityEngine;

namespace MainLeafTest.EnemyManagement.ArcherManagement
{
    [CreateAssetMenu(fileName = "New Archer Stats", menuName = "Enemy Stats/Archer Stats")]
    public class ArcherStats : EnemyStats
    {
        [SerializeField] private int _bowForce = 75;

        public int BowForce => _bowForce;
    }
}