﻿namespace MainLeafTest.GameManagement
{
    public struct PlayerPrefsKeys
    {
        public const string MATCH_TIME = "MatchTime";
        public const string INITIAL_COUNTDOWN = "InitialCountdown";
        public const string MUSIC_VOLUME = "MusicVolume";
        public const string SFX_VOLUME = "SFXVolume";

        public const string HIGH_SCORE = "HighScore";
    }
}
