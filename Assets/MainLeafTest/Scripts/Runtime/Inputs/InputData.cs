﻿using UnityEngine;

namespace MainLeafTest.InputManagement
{
    public struct InputData
    {
        public Vector2 PlayerMovement;
        public Vector2 PlayerLook;
        public bool JumpInput;
        public bool PrepareArrowInput;
        public bool ShootInput;
        public bool PauseInput;
    }
}