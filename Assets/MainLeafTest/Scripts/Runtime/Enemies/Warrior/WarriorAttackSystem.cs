﻿using System;
using UnityEngine;
using MainLeafTest.GameManagement.CollisionManagement;
using MainLeafTest.HealthManagement;

namespace MainLeafTest.EnemyManagement.WarriorManagement
{
    public class WarriorAttackSystem : EnemyAttackSystem
    {
        public event Action OnWarriorAttack;
        public event Action OnWarriorAttackMissed;
        public event Action OnWarriorAttackLanded;
        public event Action OnWarriorAttackEnded;

        [SerializeField] private Warrior _warrior = default;
        [SerializeField] private Transform _attackPoint = default;

        private bool _playerInRange;
        
        private LayerMask _playerLayer;

        protected override void InitializeVariables()
        {
            base.InitializeVariables();
            Enemy = _warrior;
            _playerLayer = LayerMask.GetMask(LayerMask.LayerToName((int) CollisionLayers.PLAYER));
        }

        protected override void Attack()
        {
            base.Attack();
            
            OnWarriorAttack?.Invoke();
        }
        
        private void Start()
        {
            InitializeVariables();
        }

        private void Update()
        {
            if (_warrior.WarriorHealthSystem.IsDead)
            {
                return;
            }
            
            CheckPlayerDistance();
        }

        private void CheckPlayerDistance()
        {
            if (Vector3.Distance(transform.position, _warrior.Player.transform.position) <=
                _warrior.WarriorStats.RangeToAttack && CanAttack)
            {
                Attack();
            }
        }

        private void DealDamage()
        {
            Collider[] colliders =
                Physics.OverlapSphere(_attackPoint.position, _warrior.WarriorStats.AttackArea, _playerLayer);

            bool attackLanded = false;
            foreach (Collider collider in colliders)
            {
                IDamageable damageable = collider.GetComponent<IDamageable>();

                if (damageable == null)
                {
                    continue;
                }
                
                damageable.TakeDamage(_warrior.WarriorStats.Damage);
                attackLanded = true;
                OnWarriorAttackLanded?.Invoke();
            }

            if (!attackLanded)
            {
                OnWarriorAttackMissed?.Invoke();
            }
        }

        private void AttackAnimationEnded()
        {
            OnWarriorAttackEnded?.Invoke();
        }
    }
}
