﻿using MainLeafTest.CameraShakeManagement;
using UnityEngine;

namespace MainLeafTest.PlayerManagement
{
    public class PlayerCameraShakeManager : MonoBehaviour
    {
        [SerializeField] private Player _player = default;

        [SerializeField] private CameraShakeEventManager _cameraShakeEventManager = default;
        [SerializeField] private CameraShakeEventData _playerDamagedCameraShake = default;
        
        private void Awake()
        {
            SubscribeToEvents();
        }

        private void SubscribeToEvents()
        {
            _player.PlayerHealthSystem.OnPlayerDamaged += PlayDamagedCameraShake;
        }

        private void PlayDamagedCameraShake()
        {
            _cameraShakeEventManager.AddCameraShakeEvent(_playerDamagedCameraShake);
        }
    }
}