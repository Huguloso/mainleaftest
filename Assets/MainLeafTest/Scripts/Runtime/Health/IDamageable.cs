﻿namespace MainLeafTest.HealthManagement
{
    public interface IDamageable
    {
        void TakeDamage(int damage);
    }
}