﻿using UnityEngine;

namespace MainLeafTest.ObjectPoolerManagement
{
    [System.Serializable]
    public struct AudioPool
    {
        public AudioPoolNames PoolName;
        public GameObject Prefab;
        public int Size;
    }
}