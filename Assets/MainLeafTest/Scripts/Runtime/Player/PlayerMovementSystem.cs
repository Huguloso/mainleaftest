﻿using System;
using UnityEngine;

namespace MainLeafTest.PlayerManagement
{
    public class PlayerMovementSystem : MonoBehaviour
    {
        public event Action OnPlayerJumped;
        public event Action OnPlayerRunning;
        public event Action OnPlayerWalking;
        public event Action OnPlayerNotMovingOnGround;
        
        [SerializeField] private Player _player = default;

        private float _verticalVelocity;
        private Vector2 _movementInput;
        private Transform _cameraTransform;

        private bool _jumpInput;

        private bool _movingSlowly;
        
        private int _currentMovSpeed;
        
        public void SetJumpInput(bool jumpInput)
        {
            _jumpInput = jumpInput;
        }

        public void SetMovement(Vector2 movement)
        {
            _movementInput = movement;
        }

        private void Awake()
        {
            InitializeVariables();
            SubscribeToEvents();
        }

        private void InitializeVariables()
        {
            _cameraTransform = UnityEngine.Camera.main.transform;
            _currentMovSpeed = _player.PlayerStats.MovementSpeed;
        }

        private void SubscribeToEvents()
        {
            _player.PlayerBowSystem.OnArrowPrepare += () =>
            {
                SetCurrentMovSpeed(_player.PlayerStats.AimingMovementSpeed);
                _movingSlowly = true;
            };

            _player.PlayerBowSystem.OnArrowShot += () =>
            {
                SetCurrentMovSpeed(_player.PlayerStats.MovementSpeed);
                _movingSlowly = false;
            };
        }

        private void Update()
        {
            CalculateGravity();

            if (_jumpInput)
            {
                Jump();
            }

            Move();
        }

        
        private void CalculateGravity()
        {
            if (!_player.CharacterController.isGrounded)
            {
                _verticalVelocity += _player.PlayerStats.Gravity * Time.deltaTime;
            }
            else
            {
                _verticalVelocity = -1;
            }
        }

        private void Jump()
        {
            if (!_player.CharacterController.isGrounded)
            {
                return;
            }
            
            _verticalVelocity = _player.PlayerStats.JumpForce;

            OnPlayerJumped?.Invoke();
        }

        private void Move()
        {
            Vector3 movement = new Vector3(_movementInput.x, 0, _movementInput.y);
            
            CheckIfRunning(movement);

            Vector3 cameraForwardNormalized = _cameraTransform.forward;
            cameraForwardNormalized.y = 0;
            cameraForwardNormalized.Normalize();

            Vector3 cameraRightNormalized = _cameraTransform.right;
            cameraRightNormalized.y = 0;
            cameraRightNormalized.Normalize();
            
            movement = cameraForwardNormalized * movement.z + cameraRightNormalized * movement.x;
            
            movement *= _currentMovSpeed;
            
            movement.y = _verticalVelocity;

            _player.CharacterController.Move(movement * Time.deltaTime);
        }
        
        private void CheckIfRunning(Vector3 movement)
        {
            if (movement != Vector3.zero && _player.CharacterController.isGrounded)
            {
                if (!_movingSlowly)
                {
                    _player.Animator.SetInteger(PlayerAnimationParameters.SPEED_INT, _currentMovSpeed);
                    OnPlayerRunning?.Invoke();
                }
                else
                {
                    _player.Animator.SetInteger(PlayerAnimationParameters.SPEED_INT, _currentMovSpeed);
                    OnPlayerWalking?.Invoke();
                }
            }
            
            else if(movement == Vector3.zero || !_player.CharacterController.isGrounded)
            {
                _player.Animator.SetInteger(PlayerAnimationParameters.SPEED_INT, 0);
                OnPlayerNotMovingOnGround?.Invoke();
            }
        }
        
        private void SetCurrentMovSpeed(int newMovSpeed)
        {
            _currentMovSpeed = newMovSpeed;
        }
    }
}