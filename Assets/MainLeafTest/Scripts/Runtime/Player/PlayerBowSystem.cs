﻿using System;
using System.Collections;
using MainLeafTest.ArrowManagement;
using MainLeafTest.GameManagement.CollisionManagement;
using MainLeafTest.ObjectPoolerManagement;
using UnityEngine;

namespace MainLeafTest.PlayerManagement
{
    public class PlayerBowSystem : MonoBehaviour
    {
        public event Action OnArrowPrepare;
        public event Action OnArrowShot;
        public event Action OnArrowCollected;
        public event Action OnArrowReload;

        private readonly Vector3 _arrowRotation = new Vector3(-90, 0, 0);

        [SerializeField] private Player _player = default;
        [SerializeField] private Transform _arrowSpawnPosition = default;
        [SerializeField] private Transform _eyes = default;
        [SerializeField] private GameObject _playerModelArrow = default;
        
        private Vector3 _aimingTarget;

        private bool _preparingArrow;
        private bool _reloading;
        
        private float _forceConversionRatio;
        private float _damageConversionRatio;
        
        private bool _holdingLeftMouseButton;
        private bool _prepareArrowInput;
        private bool _shootInput;

        private int _bowForce;
        private float _bowDamageMultiplier;
        private float _bowChargeTime;
        
        private LayerMask _corpseLayer;
        
        public void SetShootInput(bool shootInput)
        {
            _shootInput = shootInput;
            
            if (shootInput)
            {
                _holdingLeftMouseButton = false;
            }
        }

        public void SetPrepareArrowInput(bool prepareArrowInput)
        {
            _prepareArrowInput = prepareArrowInput;
            
            if (prepareArrowInput)
            {
                _holdingLeftMouseButton = true;
            }
        }

        public bool CollectArrows(int arrows)
        {
            if (Arrows == _player.PlayerStats.MaxArrows)
            {
                return false;
            }

            if (Arrows == 0)
            {
                ShowPlayerArrow();
                StartCoroutine(ReloadCoroutine());
            }
            
            Arrows += arrows;
            if (Arrows > _player.PlayerStats.MaxArrows)
            {
                Arrows = _player.PlayerStats.MaxArrows;
            }

            OnArrowCollected?.Invoke();
            return true;
        }
        
        private void Awake()
        {
            InitializeVariables();
        }

        private void InitializeVariables()
        {
            Arrows = _player.PlayerStats.StartingArrows;
            _aimingTarget = new Vector3();
            
            _corpseLayer = LayerMask.GetMask(LayerMask.LayerToName((int)CollisionLayers.CORPSE));
            
            _forceConversionRatio = _player.PlayerStats.MaxBowForce / _player.PlayerStats.MaxBowChargeTime;
            _damageConversionRatio = 1 / _player.PlayerStats.MaxBowChargeTime;
        }

        private void Update()
        {
            CheckInputs();

            SetAimTarget();

            if (_preparingArrow)
            {
                BuildBowForceAndDamage();
            }
        }

        private void CheckInputs()
        {
            if (!_shootInput && !_prepareArrowInput && !_holdingLeftMouseButton)
            {
                return;
            }
            
            if (_prepareArrowInput || _holdingLeftMouseButton)
            {
                PrepareArrow();
            }
            else if (_shootInput)
            {
                Shoot();
            }
        }

        private void SetAimTarget()
        {
            if (Physics.Raycast(_eyes.position, _eyes.forward, out RaycastHit hit, 100, ~_corpseLayer))
            {
                _aimingTarget = hit.point;
            }
            else
            {
                _aimingTarget = _eyes.forward * 10000;
            }
            
            _arrowSpawnPosition.LookAt(_aimingTarget);
        }
        
        private void PrepareArrow()
        {
            if (_preparingArrow || _reloading || Arrows <= 0)
            {
                return;
            }
            
            _preparingArrow = true;
            OnArrowPrepare?.Invoke();
            _player.Animator.SetTrigger(PlayerAnimationParameters.PREPARE_ARROW_TRIGGER);
        }

        private void Shoot()
        {
            if (!_preparingArrow || Arrows <= 0)
            {
                return;
            }
            
            _preparingArrow = false;

            Arrow arrow = _player.ObjectPooler.SpawnArrowFromPool(ArrowPoolNames.PLAYER_ARROW, Vector3.zero,
                Quaternion.Euler(_arrowRotation), _arrowSpawnPosition);

            int bodyDamage = (int) (_player.PlayerStats.ArrowDamage * _bowDamageMultiplier);
            int headShotDamage = (int) (_player.PlayerStats.ArrowHeadshotDamage * _bowDamageMultiplier);
             
             
            arrow.Fire(_bowForce, bodyDamage, headShotDamage);

            Arrows--;

            OnArrowShot?.Invoke();
                
            ResetBowForce();
                
            if (Arrows > 0)
            {
                StartCoroutine(ReloadCoroutine());
            }
            else
            {
                HidePlayerArrow();
            }
        }

        private void ResetBowForce()
        {
            _bowForce = 0;
            _bowChargeTime = 0;
        }
        
        private void BuildBowForceAndDamage()
        {
            if (!(_bowChargeTime < _player.PlayerStats.MaxBowChargeTime))
            {
                return;
            }
            
            _bowChargeTime += Time.deltaTime;
            
            _bowForce = (int)(_bowChargeTime * _forceConversionRatio);

            _bowDamageMultiplier = _bowChargeTime * _damageConversionRatio;
        }

        private void HidePlayerArrow()
        {
            _playerModelArrow.SetActive(false);
        }

        private void ShowPlayerArrow()
        {
            _playerModelArrow.SetActive(true);
        }
        
        private IEnumerator ReloadCoroutine()
        {
            _reloading = true;
            _player.Animator.SetTrigger(PlayerAnimationParameters.RELOAD_ARROW_TRIGGER);
            
            OnArrowReload?.Invoke();
            
            yield return new WaitForSeconds(_player.PlayerStats.BowReloadTime);

            _reloading = false;
        }
        
        public int Arrows { get; private set; }

        public bool LowOnArrows => Arrows < _player.PlayerStats.LowArrowsTreshold;
        
    }
}