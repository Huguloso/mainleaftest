﻿using MainLeafTest.ObjectPoolerManagement;
using UnityEngine;

namespace MainLeafTest.EnemyManagement.WarriorManagement
{
    public class WarriorMovementSystem : EnemyMovementSystem, IPoolableSpawn
    {
        [SerializeField] private Warrior _warrior = default;
        
        public void OnPoolableSpawn()
        {
            _warrior.NavMeshAgent.enabled = true;
            _warrior.NavMeshAgent.speed = _warrior.WarriorStats.MovementSpeed;
        }

        protected override void Move()
        {
            if (_warrior.NavMeshAgent.enabled)
            {
                _warrior.NavMeshAgent.destination = _warrior.Player.transform.position;
            }
        }
        
        private void Awake()
        {
            InitializeVariables();
        }

        private void Start()
        {
            SubscribeToEvents();
        }

        private void InitializeVariables()
        {
            Enemy = _warrior;
            _warrior.NavMeshAgent.speed = _warrior.WarriorStats.MovementSpeed;
        }

        private void SubscribeToEvents()
        {
            _warrior.WarriorAttackSystem.OnWarriorAttack += () =>
            {
                _warrior.NavMeshAgent.speed = _warrior.WarriorStats.AttackMovementSpeed;
            };

            _warrior.WarriorAttackSystem.OnWarriorAttackEnded += () =>
            {
                _warrior.NavMeshAgent.speed = _warrior.WarriorStats.MovementSpeed;
            };

            _warrior.WarriorHealthSystem.OnEnemyDamaged += () =>
            {
                _warrior.NavMeshAgent.speed = _warrior.WarriorStats.StaggeredMovementSpeed;
            };

            _warrior.WarriorHealthSystem.OnGotHitAnimationEnded += () =>
            {
                _warrior.NavMeshAgent.speed = _warrior.WarriorStats.MovementSpeed;
            };

            _warrior.WarriorHealthSystem.OnEnemyDeath += StopMoving;
        }

        private void StopMoving()
        {
            _warrior.NavMeshAgent.enabled = false;
        }
    }
}